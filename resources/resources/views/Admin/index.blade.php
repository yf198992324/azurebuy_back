<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AZURE后台管理系统</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Access-Control-Allow-Origin" content="*">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="icon" href="favicon.ico">
	<link rel="stylesheet" href="/layui-admin/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/font_eolqem241z66flxr.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/main.css" media="all" />
</head>
<style>
	.layui-layout-admin .layui-header {
    background-color: green;
}
</style>
<body class="main_body">
	<div class="layui-layout layui-layout-admin">
		<!-- 顶部 -->
		<div class="layui-header header">
			<div class="layui-main">
				<a href="/index" class="logo">AZURE后台管理系统</a>
			    <!-- 天气信息 -->
			    <div class="weather" >
					<iframe allowtransparency="true" frameborder="0" width="180" height="36" scrolling="no" src="//tianqi.2345.com/plugin/widget/index.htm?s=3&z=3&t=0&v=0&d=3&bd=0&k=000000&f=&ltf=009944&htf=cc0000&q=1&e=1&a=1&c=54511&w=180&h=36&align=center"></iframe>

			    </div>
			    <!-- 顶部右侧菜单 -->
			    <ul class="layui-nav top_menu">
			    	<li class="layui-nav-item showNotice" id="showNotice" pc>
						<a href="javascript:;"><i class="iconfont icon-gonggao"></i><cite>系统公告</cite></a>
					</li>
					<li class="layui-nav-item lockcms" pc>
						<a href="javascript:;"><i class="iconfont icon-lock1"></i><cite>锁屏</cite></a>
					</li>
					<li class="layui-nav-item" pc>
						<a href="javascript:;">
							<img src="/layui-admin/images/face.jpg" class="layui-circle" width="35" height="35">
							<cite>{{\Auth::user()->name}}</cite>
						</a>
						<dl class="layui-nav-child">
							<dd><a href="javascript:;" data-url="/userinfo"><i class="iconfont icon-zhanghu" data-icon="icon-zhanghu"></i><cite>个人资料</cite></a></dd>
							<dd><a href="javascript:;" data-url="/changepwd"><i class="iconfont icon-shezhi1" data-icon="icon-shezhi1"></i><cite>修改密码</cite></a></dd>
							<dd><a href="/logout"><i class="iconfont icon-loginout"></i><cite>退出</cite></a></dd>
						</dl>
					</li>
				</ul>
			</div>
		</div>
		<!-- 左侧导航 -->
		<div class="layui-side layui-bg-black">
			<div class="user-photo">
				<a class="img" title="我的头像" ><img src="/layui-admin/images/face.jpg"></a>
				<p>你好！<span class="userName">{{\Auth::user()->name}}</span>, 欢迎登录</p>
			</div>
			<div class="navBar layui-side-scroll"></div>
		</div>
		<!-- 右侧内容 -->
		<div class="layui-body layui-form">
			<div class="layui-tab marg0" lay-filter="bodyTab">
				<ul class="layui-tab-title top_tab">
					<li class="layui-this" lay-id=""><i class="iconfont icon-computer"></i> <cite>后台首页</cite></li>
				</ul>
				<div class="layui-tab-content clildFrame">
					<div class="layui-tab-item layui-show">
						<iframe src="/main"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 锁屏 -->
	<div class="admin-header-lock" id="lock-box" style="display: none;">
		<div class="admin-header-lock-img"><img src="/layui-admin/images/face.jpg"/></div>
		<div class="admin-header-lock-name" id="lockUserName">{{\Auth::user()->name}}</div>
		<div class="input_btn">
			<input type="password" class="admin-header-lock-input layui-input" placeholder="请输入密码解锁.." name="lockPwd" id="lockPwd" />
			<button class="layui-btn" id="unlock">解锁</button>
		</div>
		<p>请输入“123456”，否则不会解锁成功哦！！！</p>
	</div>

	<script type="text/javascript" src="/layui-admin/layui/layui.js?v=w312321"></script>
	<script type="text/javascript" src="/layui-admin/js/nav.js?v=w312321"></script>
	<script type="text/javascript" src="/layui-admin/js/leftNav.js?v=w312321"></script>
	<script type="text/javascript" src="/layui-admin/js/index.js?v=w312321"></script>
</body>
</html>