<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AZURE后台管理系统</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/layui-admin/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/font_eolqem241z66flxr.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/user.css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.min.css" media="all" />
</head>
<style>
	.layui-table td, .layui-table th {
		word-break: keep-all;
	}
</style>
<body class="childrenBody">
	<blockquote class="layui-elem-quote news_search">
		<form action="">
			<div class="layui-inline">
				<div class="layui-input-inline">
					<input type="text" name="keyword" value="{{@$input['keyword']}}" placeholder="请输入关键字" class="layui-input search_input">
				</div>
				<div class="layui-input-inline">
					<input type="text" name="btime" value="{{@$input['btime']}}" placeholder="请选择创建开始日期"  onclick="layui.laydate({elem: this,max: laydate.now()})" class="layui-input">
				</div>
				<div class="layui-input-inline">
					<input type="text" name="etime" value="{{@$input['etime']}}" placeholder="请选择创建结束日期" onclick="layui.laydate({elem: this,max: laydate.now()})" class="layui-input">
				</div>
				<button class="layui-btn search_btn">查询</button>
			</div>
		</form>
	</blockquote>
	<div class="layui-form news_list">
	  	<table class="layui-table">
		    <thead>
				<tr>
					{{--<th><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose" id="allChoose"></th>--}}
					<th>订单号</th>
					<th>微软订单号</th>
					<th>姓名</th>
					<th>单位</th>
					<th>支付方式</th>
					<th>总价</th>
					<th>总支付</th>
					<th>创建时间</th>
					<th>支付时间</th>
					<th>状态</th>
					<th>操作</th>
				</tr>
		    </thead>
		    <tbody class="users_content">
				@foreach($orders as $k => $v)
					<tr>
						{{--<td><input type="checkbox" name="checked" lay-skin="primary" lay-filter="choose"></td>--}}
						<td>{{$v->order_num}}</td>
						<td>{{$v->soft_order_num}}</td>
						<td>{{$v->name}}</td>
						<td>{{$v->school}}</td>
						<td >{{$payway[$v->pay_way_id]}}</td>
						<td>{{$v->price}}</td>
						<td>{{$v->total_price}}</td>
						<td>{{$v->created_at}}</td>
						<td>{{$v->pay_time == null ? '--' : $v->pay_time}}</td>
						<td>

							@php
								switch ($v->pay_status) {
                                    case 0 :
									echo '未支付';
									break;
									case 1 :
									echo '支付成功';
									break;
									case 2 :
									echo '支付失败';
									break;
									case 3 :
									echo '取消订单';
									break;
									case 4 :
									echo '已注册';
									break;
									case 5 :
									echo '已充值';
									break;
									case 6 :
									echo '已确认';
									break;
                                }
							@endphp
						</td>
						{{--<td><a class="layui-btn layui-btn-mini " href="/toregist/{{$v->id}}/{{$v->user_id}}" ><i class="iconfont icon-tags"></i>注册</a></td>--}}
						<td><a class="layui-btn layui-btn-mini regist" name="{{$v->name}}" uid="{{$v->user_id}}" oid="{{$v->id}}" ><i class="iconfont icon-tags"></i>注册</a></td>
						{{--<td>--}}
							{{--@if($v->status == 0)--}}
							{{--<a class="layui-btn layui-btn-mini users_edit"><i class="iconfont icon-edit"></i> 编辑</a>--}}
								{{--<a class="layui-btn layui-btn-danger layui-btn-mini users_del" data-id="{{$v->id}}"><i class="layui-icon">&#xe6c6;</i> 审核通过</a>--}}
								{{--@else--}}
								{{--通过--}}
							{{--@endif--}}
						{{--</td>--}}
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div id="page"></div>
	<div id="pages">
		{{$orders->appends($input)->render()}}
		<ul class="pagination pagination-sm no-margin no-padding pull-right">
			{{--<li>--}}
        {{--<span data-toggle="tooltip" data-placement="bottom" title="输入页码，按回车快速跳转">--}}
            {{--第 <input type="text" class="text-center no-padding" value="{{ $users->currentPage() }}" id="customPage" data-total-page="{{ $users->lastPage() }}" style="width: 50px;"> 页 / 共 {{ $users->lastPage() }} 页--}}
        {{--</span>--}}
			{{--</li>--}}
		</ul>

	</div>
	<script type="text/javascript" src="/layui-admin/layui/layui.js?v=w312321"></script>
	<script type="text/javascript" src="/layui-admin/js/allUsers.js?v=w312321"></script>
</body>
</html>