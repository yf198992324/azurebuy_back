<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AZURE后台管理系统</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/layui-admin/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/font_eolqem241z66flxr.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/user.css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.min.css" media="all" />
</head>
{{--<style>--}}
	{{--.layui-table td, .layui-table th {--}}
		{{--word-break: keep-all;--}}
	{{--}--}}
{{--</style>--}}
<body class="childrenBody">
	<blockquote class="layui-elem-quote news_search">

		<div class="layui-inline">
			<form action="">
		    <div class="layui-input-inline">
		    	<input type="text" name="keyword" value="{{@$input['keyword']}}" placeholder="请输入关键字" class="layui-input search_input">
		    </div>
			<div class="layui-input-inline">
				<input type="text" name="time" value="{{@$input['time']}}" placeholder="请选择购买日期"  onclick="layui.laydate({elem: this,max: laydate.now()})" class="layui-input">
			</div>
			<div class="layui-input-inline">
				<select name="status" lay-verify="">
					<option value="-1">请选择审核状态</option>
					<option value="1" @if(isset($input['status']) && $input['status'] == 1) selected @endif>已通过</option>
					<option value="0" @if(isset($input['status']) && $input['status'] == 0) selected @endif>待审核</option>
					<option value="2" @if(isset($input['status']) && $input['status'] == 2) selected @endif>未通过</option>
				</select>
			</div>
		    <button class="layui-btn search_btn">查询</button>
				<a href="/downloaduser?keyword={{@$input['keyword']}}&time={{@$input['time']}}&status={{@$input['status']}}" class="layui-btn layui-btn-normal">导出</a>
			</form>
		</div>

	</blockquote>
	<div class="layui-form news_list">
	  	<table class="layui-table">
		    {{--<colgroup>--}}
				{{--<col width="50">--}}
				{{--<col width="50">--}}
				{{--<col width="18%">--}}
				{{--<col width="8%">--}}
				{{--<col width="12%">--}}
				{{--<col width="12%">--}}
				{{--<col width="100">--}}
				{{--<col width="15%">--}}
				{{--<col width="15%">--}}
				{{--<col width="15%">--}}
		    {{--</colgroup>--}}
		    <thead>
				<tr>
					{{--<th><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose" id="allChoose"></th>--}}
					<th>姓名</th>
					<th>身份证号</th>
					<th>邮箱</th>
					<th>手机号</th>
					<th>单位</th>
					<th>微软订单号</th>
					<th>域名</th>
					<th>购买时间</th>
					<th>证件照</th>
					{{--<th>审核照片</th>--}}
					<th>下载图片</th>
				</tr>
		    </thead>
		    <tbody class="users_content">
				@foreach($users as $k => $v)
					<tr>
						{{--<td><input type="checkbox" name="checked" lay-skin="primary" lay-filter="choose"></td>--}}
						<td>{{$v->name}}</td>
						<td>{{$v->id_card}}</td>
						<td>{{$v->email}}</td>
						<td>{{$v->phone}}</td>
						<td>{{$v->school}}</td>
						<td >{{$v->soft_order_num}}</td>
						<td width="20%">{{$v->domain}}</td>
						<td>{{$v->created_at}}</td>
						<td><a class="layui-btn layui-btn-mini pic" pic="{{$v->id_card_picurl}}"><i class="iconfont icon-tags"></i>查看</a></td>
						<td>
							{{--@if($v->status == 0)--}}
							{{--<a class="layui-btn layui-btn-mini users_edit"><i class="iconfont icon-edit"></i> 编辑</a>--}}
								{{--<a class="layui-btn layui-btn-warm layui-btn-mini users_del" data-id="{{$v->id}}"><i class="layui-icon">&#xe6c6;</i> 通过</a>--}}
								{{--<a class="layui-btn layui-btn-danger layui-btn-mini nopass" data-id="{{$v->id}}"><i class="layui-icon">&#xe6c6;</i> 驳回</a>--}}
								{{--@elseif($v->status == 1)--}}
								{{--通过--}}
								{{--@else--}}
								{{--驳回--}}
							{{--@endif--}}
							<a value="{{$v->id_card_picurl}}" class="layui-btn layui-btn-mini download">下载图片</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div id="page"></div>
	<div id="pages">
		{{$users->appends($input)->render()}}
		<ul class="pagination pagination-sm no-margin no-padding pull-right">
			{{--<li>--}}
        {{--<span data-toggle="tooltip" data-placement="bottom" title="输入页码，按回车快速跳转">--}}
            {{--第 <input type="text" class="text-center no-padding" value="{{ $users->currentPage() }}" id="customPage" data-total-page="{{ $users->lastPage() }}" style="width: 50px;"> 页 / 共 {{ $users->lastPage() }} 页--}}
        {{--</span>--}}
			{{--</li>--}}
		</ul>

	</div>
	<script type="text/javascript" src="/layui-admin/layui/layui.js?v=w312321"></script>
	<script type="text/javascript" src="/layui-admin/js/allUsers.js?v=w312321"></script>
</body>
</html>