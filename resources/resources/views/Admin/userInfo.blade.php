<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>个人资料--Azure后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/layui-admin/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/user.css" media="all" />
</head>
<body class="childrenBody">
	<form class="layui-form">
		<div class="user_left">
			<div class="layui-form-item">
			    <label class="layui-form-label">用户名</label>
			    <div class="layui-input-block">
			    	<input type="text" value="{{\Auth::user()->name}}" disabled class="layui-input layui-disabled">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">用户组</label>
			    <div class="layui-input-block">
			    	<input type="text" value="超级管理员" disabled class="layui-input layui-disabled">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">创建时间</label>
			    <div class="layui-input-block">
			    	<input type="text" value="{{\Auth::user()->created_at}}" disabled class="layui-input layui-disabled">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">状态</label>
			    <div class="layui-input-block">
			    	<input type="text" value="{{\Auth::user()->status ? '正常' : '禁止'}}" disabled class="layui-input layui-disabled">
			    </div>
			</div>
			{{--<div class="layui-form-item">--}}
			    {{--<label class="layui-form-label">出生年月</label>--}}
			    {{--<div class="layui-input-block">--}}
			    	{{--<input type="text" value="" placeholder="请输入出生年月" lay-verify="required|date" onclick="layui.laydate({elem: this,max: laydate.now()})" class="layui-input">--}}
			    {{--</div>--}}
			{{--</div>--}}
			{{--<div class="layui-form-item">--}}
			    {{--<label class="layui-form-label">邮箱</label>--}}
			    {{--<div class="layui-input-block">--}}
			    	{{--<input type="text" value="" placeholder="请输入邮箱" lay-verify="required|email" class="layui-input">--}}
			    {{--</div>--}}
			{{--</div>--}}

		</div>
		{{--<div class="user_right">--}}
			{{--<input type="file" name="dddd" class="layui-upload-file" lay-title="掐指一算，我要换一个头像了">--}}
			{{--<p>由于是纯静态页面，所以只能显示一张随机的图片</p>--}}
			{{--<img src="" class="layui-circle" id="userFace">--}}
		{{--</div>--}}
		{{--<div class="layui-form-item" style="margin-left: 5%;">--}}
		    {{--<div class="layui-input-block">--}}
		    	{{--<button class="layui-btn" lay-submit="" lay-filter="changeUser">立即提交</button>--}}
				{{--<button type="reset" class="layui-btn layui-btn-primary">重置</button>--}}
		    {{--</div>--}}
		{{--</div>--}}
	</form>
	<script type="text/javascript" src="/layui-admin/layui/layui.js"></script>
	<script type="text/javascript" src="/layui-admin/js/user.js"></script>
</body>
</html>