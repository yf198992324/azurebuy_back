<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AZURE后台管理系统</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/layui-admin/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/font_eolqem241z66flxr.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/user.css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.min.css" media="all" />
</head>
<style>
	.layui-table td, .layui-table th {
		word-break: keep-all;
	}
</style>
<body class="childrenBody">
	<blockquote class="layui-elem-quote news_search">
		<form action="">
			<div class="layui-inline">
				<div class="layui-input-inline">
					<input type="text" name="keyword" value="{{@$input['keyword']}}" placeholder="请输入关键字" class="layui-input search_input">
				</div>
				<button class="layui-btn search_btn">查询</button>
			</div>
		</form>
	</blockquote>
	<div class="layui-form news_list">
	  	<table class="layui-table">
		    <thead>
				<tr>
					{{--<th><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose" id="allChoose"></th>--}}
					<th>订单号</th>
					<th>微软订单号</th>
					<th>姓名</th>
					<th>azure账号</th>
					<th>azure密码</th>
					<th>azure余额</th>
					<th>手机</th>
					<th>邮箱</th>
					<th>支付方式</th>
					<th>支付金额</th>
					<th>创建时间</th>
					{{--<th>支付时间</th>--}}
					<th>状态</th>
					<th>操作</th>
				</tr>
		    </thead>
		    <tbody class="users_content">
				@foreach($orders as $k => $v)
					<tr>
						{{--<td><input type="checkbox" name="checked" lay-skin="primary" lay-filter="choose"></td>--}}
						<td>{{$v->order_num}}</td>
						<td>{{$v->soft_order_num}}</td>
						<td>{{$v->name}}</td>
						<td>{{$v->user_acount}}</td>
						<td>{{$v->user_pass}}</td>
						<td>{{$v->money}}</td>
						<td>{{$v->phone}}</td>
						<td>{{$v->email}}</td>
						<td >{{$payway[$v->pay_way_id]}}</td>
						<td>{{$v->price}}</td>
						<td>{{$v->acount_create_time}}</td>
						{{--<td>{{$v->pay_time == null ? '--' : $v->pay_time}}</td>--}}
						<td>

							@php
								switch ($v->pay_status) {
                                    case 0 :
									echo '未支付';
									break;
									case 1 :
									echo '支付成功';
									break;
									case 2 :
									echo '支付失败';
									break;
									case 3 :
									echo '取消订单';
									break;
									case 4 :
									echo '已注册';
									break;
									case 5 :
									echo '已充值';
									break;
									case 6 :
									echo '已确认';
									break;
                                }
							@endphp
						</td>
						<td>
							@if($v->pay_status == 5)
							<a class="layui-btn layui-btn-mini confirm" _token="{{csrf_token()}}" name="{{$v->name}}" uid="{{$v->user_id}}" oid="{{$v->id}}"  >
								<i class="iconfont icon-tags"></i>确认</a>
							<a class="layui-btn layui-btn-mini layui-btn-warm sendemail" _token="{{csrf_token()}}" name="{{$v->name}}" uid="{{$v->user_id}}" oid="{{$v->id}}"  >
								<i class="iconfont icon-tags"></i>发送邮件</a>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div id="page"></div>
	<div id="pages">
		{{$orders->appends($input)->render()}}
		<ul class="pagination pagination-sm no-margin no-padding pull-right">
		</ul>

	</div>
	<script type="text/javascript" src="/layui-admin/layui/layui.js?v=w312321"></script>
	<script type="text/javascript" src="/layui-admin/js/allUsers.js?v=w312321"></script>
</body>
</html>