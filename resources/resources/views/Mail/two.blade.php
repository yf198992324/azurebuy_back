<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	p{
		margin-left: 10px;
		margin-right: 10px;
	}
</style>
<body style="background: #f8f8f8">
	<!-- <div style="width: 630px;padding: 10px;margin:0 auto;background:rgb(247,246, 242)"> -->

	<table style="-webkit-font-smoothing: antialiased;font-family:'微软雅黑', 'Helvetica Neue', sans-serif, SimHei;padding:35px 50px;margin: 25px auto; background:rgb(247,246, 242); border-radius:5px" border="0" cellspacing="0" cellpadding="0" width="640" align="center">
    <tbody>
    <tr>
        <!-- <td style="color:#000;"><img width="200px" height="57px" src="javascript:;"></td> -->
    </tr>
    <tr>
        <td style="padding:0 20px">
            <hr style="border:none;border-top:1px solid #ccc;">
        </td>
    </tr>
    <tr>
        <td style="padding: 20px 20px 20px 20px;font-weight:bold;font-family:'宋体';font-size: 24px;">
            有新用户申请购买Azure，请在3个工作日内联系用户。
        </td>
    </tr>
    <tr>
        <td style="height: 50px;color: white;" valign="middle">
            <div style="padding:10px 20px;border-radius:5px;background: rgb(64, 69, 77);margin-left:20px;margin-right:20px">
                <p style="word-break:break-all;line-height:23px;color:white;font-size:15px;text-decoration:none;">用户姓名：{{$user->name}}</p>
                <p style="word-break:break-all;line-height:23px;color:white;font-size:15px;text-decoration:none;">用户电话：{{$user->phone}}</p>
                <p style="word-break:break-all;line-height:23px;color:white;font-size:15px;text-decoration:none;">身份证号：{{$user->id_card}}</p>
                <p style="word-break:break-all;line-height:23px;color:white;font-size:15px;text-decoration:none;">支付金额：{{$money}}（{{$money/1.1}}+{{$money/11}} 服务费）</p>
                <p style="word-break:break-all;line-height:23px;color:white;font-size:15px;text-decoration:none;">购买时间：{{date('Y-m-d H:i:s')}}</p>

            </div>
        </td>
    </tr>

    <tr>
        <td style="padding: 20px 20px 20px 20px">
            -- 北京华通安信科技有限公司
        </td>
    </tr>
    <tr>
        <td style="padding:0 20px;">
            <hr style="border:none;border-top:1px solid #ccc;">
        </td>
    </tr>
    <tr>
        <td style="padding:0 20px;font-size:9pt; color:#b5b0b0">
            {{--<span style="float:left;">如果你没有进行验证操作，请不要点击此链接！</span>--}}
            <!-- <span style="float:right;">获取帮助:<a style="color:#b5b0b0" href="http://www.cernet.com " target="_blank">http://www.cernet.com</a></span> -->
        </td>
    </tr>
    </tbody>
</table>



</body>
</html>