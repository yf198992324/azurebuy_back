<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="/ext/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="/ext/font-awesome-4.5.0/css/font-awesome.min.css">

	<title>邮箱激活</title>
</head>
<body style="background: #f8f8f8">
	<div class="container">
		<div class="content" style="margin-top: 350px;text-align: center;">
				@if($flag )
					<div class="alert alert-success"><h1>恭喜您激活成功，请联系经销商进行购物体验吧！</h1></div>
				@else
					<div class="alert alert-danger"><h1>邮箱激活失败，请联系经销商进行重新验证！</h1></div>
				@endif
				<span style="color: green" onclick="webpageClose()">当前页面已经失效，3秒后窗口关闭！如果不能正常关闭，请使用电脑端或者手动关闭当前页面</span>
		</div>
	</div>
</body>
<script type="text/javascript">
	function webpageClose(){
		window.location.href="about:blank";
  		window.close();
	}
	setTimeout( webpageClose,3000)//3s钟后关闭
</script>
</html>