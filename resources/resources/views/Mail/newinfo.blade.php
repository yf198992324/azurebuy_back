<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>有新的 Azure 注册用户</title>
</head>
<body>
<h3>管理员：</h3>
<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;有新的用户注册，<a href="http://azure.shop.edu.cn/admin/" target="_blank">请前往查看&gt;&gt;</a></div>
<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户信息</h4>
<table cellpadding="0" cellspacing="0" style="border: 1px solid #D2D2D2; box-shadow: 2px 2px 2px #D2D2D2; margin-left: 38px;">
    <thead>
    <tr>
        <th style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #0e90d2 2px solid; border-right: 1px solid #d2d2d2;">姓名</th>
        <th style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #0e90d2 2px solid; border-right: 1px solid #d2d2d2;">单位</th>
        <th style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #0e90d2 2px solid; border-right: 1px solid #d2d2d2;">邮箱</th>
        <th style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #0e90d2 2px solid; border-right: 1px solid #d2d2d2;">电话</th>
        <th style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #0e90d2 2px solid; border-right: 1px solid #d2d2d2;">微软订单号</th>
        <th style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #0e90d2 2px solid; border-right: 1px solid #d2d2d2;">注册时间</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #d2d2d2 1px solid; border-right: 1px solid #d2d2d2;">{{$res->name}}</td>
        <td style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #d2d2d2 1px solid; border-right: 1px solid #d2d2d2;">{{$res->school}}</td>
        <td style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #d2d2d2 1px solid; border-right: 1px solid #d2d2d2;">{{$res->email}}</td>
        <td style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #d2d2d2 1px solid; border-right: 1px solid #d2d2d2;">{{$res->phone}}</td>
        <td style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #d2d2d2 1px solid; border-right: 1px solid #d2d2d2;">{{$res->soft_order_num}}</td>
        <td style="height: 35px ;line-height: 35px; padding: 5px 10px; border-bottom: #d2d2d2 1px solid; border-right: 1px solid #d2d2d2;">{{$res->created_at}}</td>
    </tr>
    </tbody>
</table>
</body>
</html>