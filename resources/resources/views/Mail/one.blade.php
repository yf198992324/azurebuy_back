<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	p{
		margin-left: 10px;
		margin-right: 10px;
	}
</style>
<body style="background: #f8f8f8">
	<!-- <div style="width: 630px;padding: 10px;margin:0 auto;background:rgb(247,246, 242)"> -->

	<table style="-webkit-font-smoothing: antialiased;font-family:'微软雅黑', 'Helvetica Neue', sans-serif, SimHei;padding:35px 50px;margin: 25px auto; background:rgb(247,246, 242); border-radius:5px" border="0" cellspacing="0" cellpadding="0" width="640" align="center">
    <tbody>
    <tr>
        <!-- <td style="color:#000;"><img width="200px" height="57px" src="javascript:;"></td> -->
    </tr>
    <tr>
        <td style="padding:0 20px">
            <hr style="border:none;border-top:1px solid #ccc;">
        </td>
    </tr>
    <tr>
        <td style="padding: 20px 20px 20px 20px;font-weight:bold;font-family:'宋体';font-size: 24px;">
            亲爱的{{$user->name}}用户，您好！
        </td>
    </tr>
    <tr>
        <td valign="middle" style="line-height:24px;padding: 15px 20px;">
            您的Azure产品订单购买申请以及支付流程已经完成，我们的服务人员将在3个工作日内联系并协助您完成Azure账号后续注册流程，请注意保持您的电话畅通，并感谢您对我们服务工作的配合与支持!
            <br>
            以下是您提交的用户信息，请您核对：
        </td>
    </tr>
    <tr>
        <td style="height: 50px;color: white;" valign="middle">
            <div style="padding:10px 20px;border-radius:5px;background: rgb(64, 69, 77);margin-left:20px;margin-right:20px">
                <p style="word-break:break-all;line-height:23px;color:white;font-size:15px;text-decoration:none;">购买者姓名：{{$user->name}}</p>
                <p style="word-break:break-all;line-height:23px;color:white;font-size:15px;text-decoration:none;">身份证号：{{$user->id_card}}</p>
                <p style="word-break:break-all;line-height:23px;color:white;font-size:15px;text-decoration:none;">支付金额：{{$money}}（{{$money/1.1}}+{{$money/11}} 服务费）</p>
                <p style="word-break:break-all;line-height:23px;color:white;font-size:15px;text-decoration:none;">联系电话：{{$user->phone}}</p>

            </div>
        </td>
    </tr>
    <tr>
        <td style="padding: 20px 20px 20px 20px">
            <p style="margin: 8px 0;">如您在购买及注册过程中有任何疑问，请拨打客服热线：400-686-9667</p>
            <p>(周一至周五 9:00-17:15, 节假日除外，或发邮件至<a style="color:#5083c0;text-decoration:none" href="mailto:service@shop.edu.cn" target="_blank">service@shop.edu.cn</a>)
            </p>
        </td>
    </tr>
    <tr>
        <td style="padding: 20px 20px 20px 20px">
            -- 北京华通安信科技有限公司
        </td>
    </tr>
    <tr>
        <td style="padding:0 20px;">
            <hr style="border:none;border-top:1px solid #ccc;">
        </td>
    </tr>
    <tr>
        <td style="padding:0 20px;font-size:9pt; color:#b5b0b0">
            {{--<span style="float:left;">如果你没有进行验证操作，请不要点击此链接！</span>--}}
            <!-- <span style="float:right;">获取帮助:<a style="color:#b5b0b0" href="http://www.cernet.com " target="_blank">http://www.cernet.com</a></span> -->
        </td>
    </tr>
    </tbody>
</table>



</body>
</html>