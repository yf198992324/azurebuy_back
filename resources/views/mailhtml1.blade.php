<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
    </head>
    <body>
        <p style="text-align: left;">尊敬的微软教育用户：</p>
<!--         <div style="margin-left: 30px;">
            <p>感谢您对微软教育的关注与支持！</p>

            <p>我们已经收到的购买申请，您的充值金额为'.$money.'元，您可以获得的Azure 资源为'.$money.'*90%为 '.$money*0.9.'元。</p>
            <br>
            <hr> 
            <br>
            <p>我们接下来会通过电话联系您为您开通Azure 资源，资源开通后有效期为12个月，请留意接听客服电话!</p>
            <br>
            <p>在此, 为了帮助您更好的了解Azure的相关技术资源，给您提供以下学习微软Azure的资源网站，希望对您有帮助</p>
            <p>1. 微软教育mspil网站视频学习地址:https://www.mspil.cn/#/lesson/9</p>
            <p>2. Azure直播课堂: https://www.azure.cn/zh-cn/webinars/</p>
            <p>3. Azure上云须知: https://www.azure.cn/zh-cn/webinars/ </p>
            <p>4. Azure简介: https://docs.microsoft.com/zh-cn/learn/azure/</p>
            <br>
            <p>如果您有任何问题，可以直接回复邮件，我们会尽快答复您！</p>
            <br>
            <p>同时，您可以通过关注微软教育公众号和微软学生汇公众号了解更多关于微软教育的解决方案以及客户案例，期待您的关注!</p>
            <br>
            <p>
                <img src="/images/weix1.png">
            </p>
        </div> -->
        <div style="margin-left: 30px;">
            <p>感谢您对微软教育的关注与支持！</p>
            <p>我们已经收到的购买申请，您的充值金额为**元，您可以获得的Azure 资源为**90%为*元</p>
            <p>您现有的子订阅账号：****@hnVocational.partner.onmschina.cn</p>
            <p>子订阅有效期: 2019年1月4日至2020年4月4日(延长12个自然月)</p>
            <p>子订阅总共额度: 10000+*元</p>
            <br>
            <p>特别提醒:</p>
            <p>1.我们会在您使用额度达到10000元的时候进行邮件提醒</p>
            <p>2.为了防止在不经意的情况下超额使用Azure资源造成不必要的损失，系统管理员会针对超额使用的用户的资源使用进行关闭和停止等操作，我们会以邮件或者电话方式提前通知用户，如果连续3个工作日无法联系到您，我们有权对分配给您的子订阅进行关闭等操作,子订阅关闭意味所有数据全部删除，无法保留</p>
            <p>3.试用Azure资源仅限于测试目的，请勿存放敏感核心等业务数据</p>
            <br>
            <p>在此, 为了帮助您更好的了解Azure的相关技术资源，给您提供以下学习微软Azure的资源网站，希望对您有帮助</p>
            <p>1. 微软教育mspil网站视频学习地址:https://www.mspil.cn/#/lesson/9</p>
            <p>2. Azure直播课堂: https://www.azure.cn/zh-cn/webinars/</p>
            <p>3. Azure上云须知: https://www.azure.cn/zh-cn/webinars/ </p>
            <p>4. Azure简介: https://docs.microsoft.com/zh-cn/learn/azure/</p>
            <br>
            <p>如果您有任何问题，可以直接回复邮件，我们会尽快答复您！</p>
            <br>
            <p>同时，您可以通过关注微软教育公众号和微软学生汇公众号了解更多关于微软教育的解决方案以及客户案例，期待您的关注!</p>
            <p>
                <img src="/images/weix1.png">
            </p>

        </div>
    </body>
</html>
