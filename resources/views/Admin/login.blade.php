<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <title>AZURE后台管理系统</title>
    <link rel="stylesheet" href="/layui-admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layui-admin/css/login.css">
</head>
<body class="layui-bg-cyan">
<div class="admin-login-block">
    <div class="admin-login-text">AZURE <span>后台管理系统</span></div>
    <div class="login-font"><span style="color: red">
            @if(isset($status) && $status == -1)
                密码或者用户名错误，请重试！
                @endif
        </span></div>
    <div class="admin-login-form">
        <form class="layui-form" action="/postlogin" method="post" lay-filter="login">
            {{csrf_field()}}
            <div class="layui-form-item">
                <div class="layui-input-block admin-login-input"><input type="text" name="username" required lay-verify="required" placeholder="登录账号"   autocomplete="off" class="layui-input"></div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block admin-login-input"><input type="password" name="password" required lay-verify="required" placeholder="登录密码" autocomplete="off" class="layui-input"></div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block admin-login-input">
                    <button class="layui-btn admin-login-btn" lay-submit>登录</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="/layui-admin/layui/layui.js"></script>
<script>
//        layui.use(["layer", "form"], function () {
//        var i = (layui.jquery, layui.layer), o = layui.form();
//        o.on("submit(login)", function () {
//    //        var o = i.load(2);
//    //        return setTimeout(function () {
//    //            i.close(o), location.href = "index.html"
//    //        }, 1e3), !1
//            alert('ss');
//        })
//    })
</script>
</body>
</html>