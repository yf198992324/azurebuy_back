<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AZURE后台管理系统</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/layui-admin/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/font_eolqem241z66flxr.css" media="all" />
	<link rel="stylesheet" href="/layui-admin/css/user.css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.min.css" media="all" />
</head>
<style>
	.layui-table td, .layui-table th {
		word-break: keep-all;
	}
</style>
<body class="childrenBody">
	<blockquote class="layui-elem-quote news_search">
		<form action="">
			<div class="layui-inline">
				<div class="layui-input-inline">
					<input type="text" name="keyword" value="{{@$input['keyword']}}" placeholder="请输入关键字" class="layui-input search_input">
				</div>
				<button class="layui-btn search_btn">查询</button>
			</div>
		</form>
	</blockquote>
	<div class="layui-form news_list">
	  	<table class="layui-table">
		    <thead>
				<tr>

					<th>姓名</th>
					<th>azure账号</th>
					<th>azure密码</th>
					<th>azure余额</th>
					<th>手机</th>
					<th>邮箱</th>
					<th>创建时间</th>
					<th>操作</th>
				</tr>
		    </thead>
		    <tbody class="users_content">
				@foreach($orders as $k => $v)
					<tr>
						<td>{{$v->name}}</td>
						<td>{{$v->user_acount}}</td>
						<td>{{$v->user_pass}}</td>
						<td>{{$v->money}}</td>
						<td>{{$v->phone}}</td>
						<td>
							{{$v->email}}
							<a class="layui-btn layui-btn-mini sss"  uid="{{$v->user_info_id}}" name="{{$v->name}}"  email="{{$v->email}}" >
								<i class="iconfont icon-tags"></i>修改</a>
						</td>
						<td>{{$v->created_at}}</td>
						<td>
								<a class="layui-btn layui-btn-mini layui-btn-warm sendemail" _token="{{csrf_token()}}" name="{{$v->name}}" uid="{{$v->userid}}" oid="{{$v->id}}"  >
									<i class="iconfont icon-tags"></i>发送邮件</a>
						</td>

					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div id="page"></div>
	<div id="pages">
		{{$orders->appends($input)->render()}}
		<ul class="pagination pagination-sm no-margin no-padding pull-right">
		</ul>

	</div>
	<script type="text/javascript" src="/layui-admin/layui/layui.js?v=w312321"></script>
	<script type="text/javascript" src="/layui-admin/js/allUsers.js?v=w312321"></script>
</body>
</html>