<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AZURE</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<script src="/js/jquery.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
	<div class="row">

		<div class="col-sm-12 col-md-12 col-lg-12" style="background: rgb(41,156,222); color: #ffffff; margin-top: 10px;">
			<h4><span style="color: red;">*</span> 北京华通安信科技有限公司为您服务</h4>
			<p> 我们是微软教育行业此次活动的服务提供商。提供公有云业务购买的相关支持，所涉服务及票据费用为购买金额的10%。 <br>
				用户最终使用的 Azure 产品所属公有云服务均为世纪互联运营。</p>
		</div>
	</div>
	<style type="text/css">
		table.selftable{
			border-collapse: collapse;
			border: 0px solid #999;
			margin: auto;
		}

		table.selftable td {
			border-top: 0;
			border-right: 2px solid #999;
			border-bottom: 2px solid #999;
			border-left: 0;
			width: 180px;
			text-align: center;
			padding: 10px;
			vertical-align: top;
		}

		table.selftable tr.lastrow td {
			border-bottom: 0;
		}

		table.selftable tr td.lastCol {
			border-right: 0;
		}
		table.selftable tr td.lastRow {
			border-bottom: 0;
		}
		table.selftable tr td p {
			margin-top: 10px;
		}
	</style>
	<div style="width: 1000px; margin: auto; height: auto;">
		<h5 style="text-align: left; width: 100%; line-height: 70px;"><span style="color: red;">*</span> 您充值成功后，可以根据自己需要自行选择以下服务。</h5>
		<img src="/images/service.jpg" width="100%">
		<br>
		<br>
		<a href="second" class="btn btn-primary" style="float: right;">确认下一步</a>
	</div>


	<div style="clear: both;"></div><br>
	<div style="height: 50px; width: 100%; border-top: 1px solid #cccccc; padding-top: 20px; font-size: 12px; color: #999999; line-height: 	15px;">
        <p>京ICP备12014130号-1 © 2005-2019 版权所有，并保留所有权利</p>
		<!-- <p>支持部门: 中华人民共和国教育部科技发展中心</p> -->
<!-- 		<p>服务提供商: 赛尔网络 CERNET</p>
 -->		<p><a href="/images/ace2.png" target="_blank">微软AEP资质证书</a></p>
	</div>
</div>
</body>
</html>