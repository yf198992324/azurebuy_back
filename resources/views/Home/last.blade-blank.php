<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<title>test</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">  
	<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	
<div class="container">
	<div class="row">

      <div class="col-sm-12 col-md-12 col-lg-12">
			<h2>确认付款消息</h2>    
			<p> 个人及联系信息</p> 
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12" style="box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;">

			<table class="table" style="margin: 10px 10px" id="s">
			<style type="text/css">
				#s * {
					 border: 0px solid transparent !important;
				}
			</style>
			  <tbody>

			    <tr>
			      <td>姓名</td>
			      <td>张三</td>
			    </tr>

			    <tr>
			      <td>单位</td>
			      <td>xx大学</td>
			    </tr> 

			    <tr>
			      <td>邮箱</td>
			      <td>zhangsan@xxdax.edu.cn</td>
			    </tr> 

			    <tr>
			      <td>申请域名:</td>
			      <td></td>
			    </tr>

			    <tr>
			      <td></td>
			      <td>xxdax.partner.onmschina.cn</td>
			    </tr> 

			    <tr>
			      <td></td>
			      <td>xxdax.partner.onmschina.cn</td>
			    </tr> 

			    <tr>
			      <td></td>
			      <td>xxdax.partner.onmschina.cn</td>
			    </tr> 

			    <tr>
			      <td></td>
			      <td>xxdax.partner.onmschina.cn</td>
			    </tr>

			  </tbody>
			</table>

		</div>
	</div>
	<p style="margin: 10px 1px"><strong style="font-size: 20px;">您选购的微软套餐：入门级套餐</strong></p>
	<div class="row">
	<div class="table-responsive">                 
	<table class="table table-striped table-bordered">
		<thead>
		<tr>
			<th style="text-align: center;">服务内容</th>
			<th>型号</th>
			<th>备注</th>
			<th>数量</th>
			<th>价格</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td style="text-align: center;">虚拟机服务</td>
			<td>A2 v2</td>
			<td >2个内核，4GB RAM，200GB磁盘，带宽200Mbps</td>
			<td>1000小时</td>
			<td>¥1081.00</td>
		</tr>
		<tr>
			<td rowspan ="4" style="text-align: center; vertical-align:middle;">存储服务</td>
			<td>磁盘存储</td>
			<td>虚拟机的磁盘存储，按实际使用收费0.44元/GB/月,<br>包含3个月1000GB费用，不使用不收费</td>
			<!-- <td>2个内核，4GB RAM，200GB磁盘，带宽200Mbps</td> -->
			<td>1000GB</td>
			<td>¥1320.00</td>
		</tr>
		<tr>
			<td>磁盘存储访问</td>
			<td>虚拟机的磁盘存储的访问<br>和操作，按实际使用收费，超出的部分0.44元/百万次</td>
			<td>12000万次访问</td>
			<td>¥53.00</td>
		</tr>
		<tr>
			<td>块存储(热)</td>
			<td>文件存储，按实际使用收<br>费，超出部分按0.149/GB/月</td>
			<td>5000GB</td>
			<td>¥2235.00</td>
		</tr>
		<tr>
			<td>块存储访问</td>
			<td>文件存储访问和操作，按<br>实际使用收费，9元/百万次</td>
			<td>12000万次访问</td>
			<td>¥1080.00</td>
		</tr>
		<tr>
			<td rowspan="2" style="text-align: center; vertical-align:middle;">网络服务
			</td>
			<td>传入数据</td>
			<td>每月包含1000GB的<br>入栈传输流量，超过的按0.67元每GB。</td>
			<td>1000GB/月</td>
			<td>¥0.00</td>
		</tr>
		<tr>
			<td>传出数据</td>
			<td>总共包含1000GB的出战传输<br>流量，按实际使用量计费，超出按0.67元/GB</td>
			<td>1000</td>
			<td>¥670.00</td>
		</tr>
		</tbody>
	</table>
	</div>
	</div>

	<p style="margin: 10px 1px"><strong style="font-size: 15px;">选择支付方式</strong></p>
	<div class="row">
		<div class="col-md-4">
			<div class="checkbox">
			    <label>
			      <input type="radio" style="margin-top: 20px" name='pay'><img src="/images/z.jpg" width="70">
			    </label>
			</div>
		</div>

		<div class="col-md-4">
			<div class="checkbox">
			    <label>
			      <input type="radio" style="margin-top: 20px" name='pay'><img src="/images/y.jpg" width="65" >
			    </label>
			</div>
		</div>

		<div class="col-md-4">
			<div class="checkbox">
			    <label>
			      <input type="radio" style="margin-top: 20px" name='pay'><img src="/images/g.jpg" width="65" >
			    </label>
			</div>
		</div>
		
	</div>
	
	<a href="#" class="btn btn-primary" style="float: right;">确认</a>
</div>

</body>
</html>