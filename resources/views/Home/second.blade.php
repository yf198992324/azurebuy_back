<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>AZURE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>
<div class ="container">

    <h3><strong>免责声明。</strong></h3>
    <!-- <pre style="height: 500px; word-break:break-all; overflow: auto;"> -->
    <div class="well well-lg" style="line-height: 30px;">
        &nbsp;&nbsp;&nbsp;&nbsp;本网站所列的产品为公有云服务产品，已取得生产厂家的合法授权。云产品为特殊商品，一经售出，不予退换。<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;本产品为北京华通安信科技有限公司代理世纪互联数据中心有限公司销售产品，客户将不定期收到产品生产厂商的促销活动的电话或者电话回访等服务。<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;本网站遵守厂商规定，仅作为产品合法销售的平台，产品质量及售后服务均由厂商提供。若商品出现质量问题，请在由世纪互联数据中心有限公司运营的产品使用平台（https://www.azure.cn/）上与厂商售后服务中心联系解决。<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;现就购买者提供的个人信息作出如下特别声明：<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;1、由于厂商要求最终用户须提供个人信息资料，我们将在您的同意及确认下，通过注册表格等形式要求您提供一些个人资料，如：您的姓名、性别、年龄、出生日期、身份证号、家庭住址、教育程度、企业情况、所属行业等。本网站保证上述资料不会以任何形式泄露给除厂商以外的第三方。<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;2、当有关机关依照法定程序要求本网站披露上述资料时，我们将依法为该机关提供个人资料。在此情况下之披露，本网站不负任何责任。<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;3、因用户将个人密码告知他人或与他人共享注册账户导致的任何个人资料泄露， <br/>
        &nbsp;&nbsp;&nbsp;&nbsp;4、任何由于黑客攻击、计算机病毒侵人或发作、因政府管制而造成的暂时性关闭等影响网络正常经营的不可抗力造成的个人资料泄露、丢失、被盗用或被窜改等，本网站不负任何责任。<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;5、由于与本网站链接的其他网站所造成之个人资料泄露及由此而导致的任何法律争议和后果，本网站均得免责。<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;6、本网站因系统维护或升级需暂停服务时，将事先公告。若因线路及非本网站控制范围外的硬件故障或其他不可抗力而导致暂停服务，于暂停服务期间造成的损失，本网站不负任何责任。<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;7、本网站使用者因为违反本声明的规定而触犯中华人民共和国法律的，一切后果自己负责，本网站不承担任何责任。<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;8、凡以任何方式登录本网站或直接、间接注册购买使用本网站产品，视为自愿接受本网站声明的约束。<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;本声明未涉及的问题参见国家有关法律法规。<br/>

    </div>
    <div class="checkbox" style="width: 100%; text-align: center;">
        <label>
            <input type="checkbox" id="agree" value="1">已阅读，并同意以上《免责声明》内容
        </label>
        <br>
        <br>
        <button class="btn btn-primary" style="margin: auto;">已阅读，并同意《免责声明》，下一步</button>
    </div>

    <!-- <a href="#" class="btn btn-primary" style="display: table;margin:10px auto"></a>
     -->
    <div style="clear: both;"></div><br>
    <div style="height: 50px; width: 100%; border-top: 1px solid #cccccc; padding-top: 20px; font-size: 12px; color: #999999; line-height: 	15px;">
        <p>京ICP备12014130号-1 © 2005-2019 版权所有，并保留所有权利</p>
        <!-- <p>支持部门: 中华人民共和国教育部科技发展中心</p> -->
<!--         <p>服务提供商: 赛尔网络 CERNET</p>
 -->        <p><a href="/images/ace2.png" target="_blank">微软AEP资质证书</a></p>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('button.btn.btn-primary').click(function () {
            if($('#agree').is(':checked')) {   // do something
                window.location.href = '/third'
            }else{
                alert('请确认您已经阅读了以上《免责声明》，并勾选上面的复选框。')
            }
        })
    })
</script>
</body>
</html>