<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>AZURE</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/layer.js"></script>

    <script src="/WebUploader/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/WebUploader/css/webuploader.css">

</head>
<body>

<style>
    .pic{
        width: 312px;
        height: 210px;
        margin-left: 24px;
        margin-top: 10px;
    }
</style>

<div class ="container">
    <div class="row">
        <h3><strong>请填写申请购买认证信息</strong></h3></div>
    <form action="" method="POST" enctype="multipart/form-data" onsubmit="return false"  id="form">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="col-sm-6">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="soft_order_num" value="{{$data['order_number']}}">
                    <div class="form-group">
                        <label for="firstname" class="col-sm-4 control-label">姓名</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="name"
                                   placeholder=""><span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-4 control-label">身份证号</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="id_card"
                                   placeholder=""><span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-4 control-label">手机号码</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="phone"
                                   placeholder="" value="{{$data['cellphone_number']}}"><span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-4 control-label">邮箱</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="email"
                                   placeholder="" value="{{$data['email']}}"><span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-4 control-label">单位名称</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="school" placeholder=""><span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain_1" class="col-sm-4 control-label">5个备用的3级域名</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" id="domain_1" class="form-control do" placeholder="" name="domain_1" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">.partner.onmschina.cn</span><span style="color: red;float: right;margin-right: -20px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain_2" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" id="domain_2" class="form-control do" placeholder="" name="domain_2" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">.partner.onmschina.cn</span><span style="color: red;float: right;margin-right: -20px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain_3" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" id="domain_3" class="form-control do" placeholder="" name="domain_3" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">.partner.onmschina.cn</span><span style="color: red;float: right;margin-right: -20px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain_4" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" id="domain_4" class="form-control do" placeholder="" name="domain_4" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">.partner.onmschina.cn</span><span style="color: red;float: right;margin-right: -20px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain_5" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" id="domain_5" class="form-control do" placeholder="" name="domain_5" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">.partner.onmschina.cn</span><span style="color: red;float: right;margin-right: -20px">*</span>
                            </div>
                        </div>
                    </div>
                </div>


            <div class="col-sm-6" >
                <div style="margin-top:22px;">
                    <h2 style="line-height: 30px;"> 服务支持：400-686-9667</h2>
                </div>
            </div>



                <div class="col-sm-6" style="height: 492px;">
                    <div style="margin-top:119px;">
                        <h4 style="line-height: 30px;"><span style="color: red;">*</span> 您需要用户 ID 和密码来登录帐户，我们会从您的备用域名中选择一个来帮您完成注册，完成注册后，您可以修改登陆密码。</h4>
                        <h4>示例：</h4>
                        <p>“清华大学” 的域名推荐为：</p>
                        <p>qinghuadaxue.partner.onmschina.cn</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="firstname" class="col-sm-2 control-label">身份证照片</label>
                    <div class="col-sm-10"></div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">
                    </div>

                    <div class="col-sm-10">
                        <div class="col-md-6">


                            <div id="uploader-demo" style="margin-bottom: 30px">
                                <!--用来存放item-->
                                <div id="fileList" class="uploader-list"></div>
                                <div id="filePicker">选择图片</div>
                            </div>
                            <input type="hidden" name="WU_FILE_0">
                            <input type="hidden" name="WU_FILE_1">


                        </div>
                        <div class="col-md-6" >
                            <div class="row" style="margin-top: 10px;">
                                <div style="margin-left: 10px;margin-right: 5px">
                                    <p style="font-weight: bolder;">图例及要求</p>
                                    <p>每张图片不得超过 <span STYLE="color: red;font-weight: bold;font-size: larger">2M</span>，支持JPG，JPEG，PNG三类图片类型格式；调整好光纤，文字清晰可见证件:显示完整，占据图像面基80%以上。</p>
                                    <img src="/images/1.jpg" width="304"  style="margin:5px auto">
                                    <img src="/images/2.jpg"  width="304"  style="margin:5px auto">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <button class="btn btn-primary" type="button" style="display: table;margin:10px auto; padding: 15px; width: 300px;" id="sub">提交</button>
    </form>
    <br>
    <input type="hidden" id="num" value="1">
    <div style="clear: both;"></div><br>
    <div style="height: 50px; width: 100%; border-top: 1px solid #cccccc; padding-top: 20px; font-size: 12px; color: #999999; line-height: 	15px;">
        <p>京ICP备12014130号-1 © 2005-2018 版权所有，并保留所有权利</p>
        <p>支持部门: 中华人民共和国教育部科技发展中心</p>
        <p>服务提供商: 赛尔网络 CERNET</p>
        <p><a href="./ace.jpg" target="_blank">微软ACE资质证书</a></p>
    </div>
</div>
</body>
</html>
<script type="text/javascript">

    $('#sub').bind('click',function () {
        var re = checkempty();
        if(!re && re != undefined) return false;
        $(this).unbind('click');
        var data = new FormData($('#form')[0]);
        $.ajax({
            url: '/third',
            type: 'POST',
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#sub").html("正在上传...");
                $("#sub").attr("class",'btn-danger');
                $('#sub').attr('disable','disable');

                layer.load(0, {
                    shade: [0.7,'#fff'] //0.1透明度的白色背景
                });
            },
            complete: function () {
// //                layer.closeAll();
//                 window.location.href='/third';
            }
        }).done(function (e) {
            if(e == 0){
                layer.alert('请上传png/jpg/gif格式的图片！');
                return false;
            }else if(e == -1){
                layer.alert('提交失败！');
                return false;
            }else if(e == -2){
                layer.alert('订单已存在，请勿重复提交!');
                return false;
            }else{
                layer.alert('提交成功！', {
                    icon:6,
                    closeBtn:0,
                    title:false
                },function () {
                    window.location.href='/last/'+e+'?s=done';
                });
            }
        });
    });


    function checkempty() {
        var pic_one = $("input[name='WU_FILE_0']").val();
        if(pic_one == ''){
            layer.alert('请上传正面身份证照片!');
            return false;
        }
        var pic_two = $("input[name='WU_FILE_1']").val();
        if(pic_two == ''){
            layer.alert('请上传背面身份证照片!');
            return false;
        }
        var name = $("input[name='name']").val();
        if(name != undefined && name == ''){
            layer.alert('请填写姓名');
            return false;
        }
        var id_card = $("input[name='id_card']").val();
        var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        if(id_card == '' || !reg.test(id_card)){
            layer.alert('请填写正确的身份证号码!');
            return false;
        }
        var phone = $("input[name='phone']").val();
        var reg = /^1(3|4|5|7|8)\d{9}$/;
        if(phone == '' || !reg.test(phone)){
            layer.alert('请填写正确的手机号码!');
            return false;
        }
        var email = $("input[name='email']").val();
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
        if(email == '' || !reg.test(email)){
            layer.alert('请填写正确的邮箱地址!');
            return false;
        }
        var school = $("input[name='school']").val();
        if(school == '' ){
            layer.alert('请填写单位名称!');
            return false;
        }
        var num = 0;
        $('.do').each(function(k,v){
            if($(this).val() != ''){
                num++;
            }
        });
        if(num != 5){
            layer.alert('请填写5个备用的3级域名!');
            return false;
        }

    }

jQuery(function() {
        var $ = jQuery,
            $list = $('#fileList'),
            // 优化retina, 在retina下这个值是2
            ratio = window.devicePixelRatio || 1,

            // 缩略图大小
            thumbnailWidth = 312 * ratio,
            thumbnailHeight = 210 * ratio,

            // Web Uploader实例
            uploader;

        // 初始化Web Uploader
       var uploader = WebUploader.create({

            // 自动上传。
            auto: true,

            // swf文件路径
            swf:  '/WebUploader/js/Uploader.swf',

            // 文件接收服务端。
            server: '/doUpload',

            // [默认值：'file']  设置文件上传域的name。
            fileVal:'upload',

            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '#filePicker',

            // 单个图片最大2M
            fileSingleSizeLimit:2*1024*1024,
            // 允许最大上传2张照片
            fileNumLimit:10,

            // 只允许选择文件，可选。
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,png',
                mimeTypes: 'image/jpg,image/jpeg,image/png,image/gif'
            }
        });

        // 当有文件添加进来的时候
        uploader.on( 'fileQueued', function( file ) {
            var num = $('#num').val();
            if(num>=3){
                alert('最大只能上传2张');
                return false;
            }
            var $li = $(
                '<div id="' + file.id + '" class="file-item thumbnail">' +
                '<img><button onclick="return del('+ file.id + ')">删除</button>' +
//                '<img><button onclick="return del()">删除</button>' +
//                '<img><button class="xx">删除</button>' +
                '<div class="info">' + file.name + '</div>' +
                '</div>'
                ),
                $img = $li.find('img');

            $list.append( $li );
            num++;
            $('#num').val(num);
            // 创建缩略图
            uploader.makeThumb( file, function( error, src ) {
                if ( error ) {
                    $img.replaceWith('<span>不能预览</span>');
                    return;
                }

                $img.attr( 'src', src );
            }, thumbnailWidth, thumbnailHeight );
        });

        // 文件上传过程中创建进度条实时显示。
        uploader.on( 'uploadProgress', function( file, percentage ) {
            var $li = $( '#'+file.id ),
                $percent = $li.find('.progress span');

            // 避免重复创建
            if ( !$percent.length ) {
                $percent = $('<p class="progress"><span></span></p>')
                    .appendTo( $li )
                    .find('span');
            }

            $percent.css( 'width', percentage * 100 + '%' );
        });

        // 文件上传成功，给item添加成功class, 用样式标记上传成功。
        uploader.on( 'uploadSuccess', function( file,data ) {
            //var fileList = $.parseJSON(callBack);//转换为json对象
//            alert(data);
//            alert('upload success\n'+data.name);
            var imgurl = data.url; //上传图片的路径
//            alert(imgurl);
//            alert(file.id);
//            $('#uploader-demo').append('<input  type="hidden" name="pic"+id value="'+imgurl+'"/>');
            $("input[name='"+file.id+"']").val(imgurl);
            $( '#'+file.id ).addClass('upload-state-done');
            if(file.id == 'WU_FILE_0'){
                $('#uploader-demo').append('<a href="javascript:location.reload();" class="btn btn-danger" style="margin-top: 20px">重新上传</a>');
            }
        });

        // 文件上传失败，现实上传出错。
        uploader.on( 'uploadError', function( file ) {

            var $li = $( '#'+file.id ),
                $error = $li.find('div.error');

            // 避免重复创建
            if ( !$error.length ) {
                $error = $('<div class="error"></div>').appendTo( $li );
            }

            $error.text('上传失败!!!');
        });

        /**
         * 验证文件格式以及文件大小
         */
        uploader.on("error", function (type) {
           // alert(type);
            if (type == "Q_TYPE_DENIED") {
                layer.msg("请上传JPG、PNG、GIF、BMP格式文件",{icon:6});
            }
            else if (type == "F_EXCEED_SIZE") {
                layer.msg("文件大小不能超过2M",{icon:5});
            }
            else if (type == "Q_EXCEED_NUM_LIMIT") {
//                layer.msg("请上传一张正面，一张反面身份证照片",{icon:5});
                layer.msg("只允许上传两张照片，请按要求上传！",{icon:5});
            }
            else if (type == "F_DUPLICATE") {
                layer.msg("请不要重复上传!",{icon:5});
            }
            else {
                layer.msg("上传出错！请检查后重新上传！错误代码"+type);
            }
        });
    
        // 完成上传完了，成功或者失败，先删除进度条。
        uploader.on( 'uploadComplete', function( file ) {
            $( '#'+file.id ).find('.progress').remove();
        });

    });
    
    
    function  del(dd) {
        var num = $('#num').val();
        var name = $(dd).attr('name');
        $('#'+name).remove();
        $('#num').val(num-1);
        // uploader.trigger('fileDequeued');
    }



</script>