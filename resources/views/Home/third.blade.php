<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>AZURE</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/layer.js"></script>
    <script src="/js/laydate.js"></script>

    <script src="/WebUploader/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/WebUploader/css/webuploader.css">

</head>
<body>
<div class ="container">
    <div class="row">
        <h3><strong>请填写申请购买认证信息</strong></h3></div>
    </div>
{{--1.	机构名称--}}
{{--2.	电话，--}}
{{--3.	联系人，--}}
{{--4.	邮箱，--}}
{{--5.	预计购买时间--}}
{{--6.	第一次充值的金额--}}

    <form action="" class="form-horizontal" method="POST" enctype="multipart/form-data" onsubmit="return false"  id="form">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="col-sm-6">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="soft_order_num" value="{{$data['order_number']}}">
                <div class="form-group">
                    <label for="firstname" class="col-sm-4 control-label">机构名称</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="school" placeholder="">
                        <span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="firstname" class="col-sm-4 control-label">电话</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="phone" placeholder="" value="{{$data['cellphone_number']}}">
                        <span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="firstname" class="col-sm-4 control-label">联系人</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="name" placeholder="">
                        <span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="firstname" class="col-sm-4 control-label">邮箱</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="email" placeholder="" value="{{$data['email']}}">
                        <span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="buytime" class="col-sm-4 control-label">预计购买时间</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="buytime"  id="buytime" placeholder="">
                        <span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                    </div>
                    
                </div>

               <!--  <div class="form-group">
                    <label for="money" class="col-sm-4 control-label">充值的金额</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="money" id="money" placeholder="">
                        <span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                    </div>
                </div> -->

                <div class="form-group" id = 'inactivestyle'>
                    <label for="inactive" class="col-sm-4 control-label">是否参加过“Azure 3个月1万元试用”体验活动</label>
                    <div class="col-sm-8">
                        <label class="radio-inline" for="inactive1">
                          <input type="radio" name="inactive" id="inactive1" value="1"> 参加过
                        </label>
                        <label class="radio-inline" for="inactive2">
                          <input type="radio" name="inactive" id="inactive2" checked="checked" value="2"> 没有参加过
                        </label>
                    </div>
                </div>
                <div class="form-group" id="rechargestyle">
                </div>
               <!--  <div class="form-group">
                    <label for="money" class="col-sm-4 control-label">请填写参加活动的Azure账号</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="activeazureaccount" id="activeazureaccount" disabled placeholder="">
                    </div>
                </div> -->

                <div class="form-group" id="moneystyle">
                   
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label"></label>
                    <div class="col-sm-8">
                        <button class="btn btn-primary" type="button" style="display: table;margin:10px auto; padding: 15px; width: 300px;" id="sub">提交</button>
                    </div>
                </div>

                <script type="text/javascript">
                	$(function(){
						// $('input[type=radio][name=bedStatus]').change(function() {
						// 	if (this.value == '1') {

						//  	}else if (this.value == 'transfer') {
						//  		alert("Transfer Thai Gayo");
						//  	}
						// });
						// $('input:radio[name=sex]:checked').val();
                	});
                </script>
            </div>
        </div>

    </form>
    <br>
    <input type="hidden" id="num" value="1">
    <div style="clear: both;"></div><br>
    <div style="height: 50px; width: 100%; border-top: 1px solid #cccccc; padding-top: 20px; font-size: 12px; color: #999999; line-height: 	15px;">
        <p>京ICP备12014130号-1 © 2005-2019 版权所有，并保留所有权利</p>
        <!-- <p>支持部门: 中华人民共和国教育部科技发展中心</p> -->
        <!-- <p>服务提供商: 赛尔网络 CERNET</p> -->
         <p><a href="/images/ace2.png" target="_blank">微软AEP资质证书</a></p>
    </div>

<script>
    laydate.render({
      elem: '#buytime' //指定元素
      ,type: 'datetime' 
    });
    var rechargehtml = '<label for="recharge" class="col-sm-4 control-label">是否直接充值在现有的试用账号上</label>'+
                    '<div class="col-sm-8" >'+
                        '<label class="radio-inline" for="recharge1">'+
                          '<input type="radio" name="recharge" id="recharge1" value="1"> 是'+
                        '</label>'+
                        '<label class="radio-inline" for="recharge2">'+
                          '<input type="radio" name="recharge" id="recharge2" value="2"> 否'+
                        '</label>'+
                    '</div>';
    var moneyhtml = '<label for="money" class="col-sm-4 control-label">请填写需要充值的Azure 试用体验账号</label>'+
                    '<div class="col-sm-8">'+
                        '<input type="text" class="form-control"  name="moneytoazureaccount"  id="moneytoazureaccount" placeholder="">'+
                    '</div>';
    $('#inactive1').on('click', function(){
        $('#rechargestyle').empty().html(rechargehtml);
    });
    $('#inactive2').on('click', function(){
        $('#rechargestyle').empty();
        $('#moneystyle').empty();
    });
    $('body').on('click', "#recharge1",function(){
         $('#moneystyle').empty().html(moneyhtml);
    });
    $('body').on('click', "#recharge2",function(){
         $('#moneystyle').empty();
    });
    $('#sub').bind('click',function () {
        var re = checkempty();
        if(!re && re != undefined) return false;
        $(this).unbind('click');
        var data = new FormData($('#form')[0]);
        $.ajax({
            url: '/third',
            type: 'POST',
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            complete: function () {
// //                layer.closeAll();
//                 window.location.href='/third';
            }
        }).done(function (e) {
            if(e.status == 'errors'){
                if(e.res == -1){
                    layer.alert('提交失败！');
                    return false;
                }else if(e.res == -2){
                    layer.alert('订单已存在，请勿重复提交!');
                    return false;
                }
            }else if(e.status == 'success'){
                    layer.alert('提交成功！', {
                    icon:6,
                    closeBtn:0,
                    title:false
                },function () {
                    window.location.href='/last/'+e.res+'?s=done';
                });
            }else{
                layer.alert('提交失败！');
                return false;
            }
        });
    });

    function checkempty() {
        var name = $("input[name='name']").val();
        if(name != undefined && name == ''){
            layer.alert('请填写姓名');
            return false;
        }

        var phone = $("input[name='phone']").val();
        var reg = /^1(3|4|5|7|8)\d{9}$/;
        if(phone == '' || !reg.test(phone)){
            layer.alert('请填写正确的手机号码!');
            return false;
        }
        var email = $("input[name='email']").val();
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
        if(email == '' || !reg.test(email)){
            layer.alert('请填写正确的邮箱地址!');
            return false;
        }
        var school = $("input[name='school']").val();
        if(school == '' ){
            layer.alert('请填写机构名称!');
            return false;
        }
        var buytime = $("input[name='buytime']").val();
        if(buytime == '' ){
            layer.alert('请选择预计购买时间!');
            return false;
        }
        var inactive = $('input:radio[name="inactive"]:checked').val();
        if(inactive == 1){
             var recharge = $('input:radio[name="recharge"]:checked').val();
             if(recharge == 1){
                var moneytoazureaccount = $("input[name='moneytoazureaccount']").val();
                if(moneytoazureaccount == ''){
                     layer.alert('请填写体验账号!');
                     return false;
                }
             }else if(recharge == 2){

             }else{
                layer.alert('请选择是否直接充值在现有的试用账号!');
                return false;
             }
        }

    }
</script>
</body>
</html>