<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>AZURE</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/layer.js"></script>
</head>
<body>

<style>
    .pic{
        width: 312px;
        height: 210px;
        margin-left: 24px;
        margin-top: 10px;
    }
</style>

<div class ="container">
    <div class="row">
        <h3><strong>请填写申请购买认证信息</strong></h3></div>
    <form action="" method="POST" enctype="multipart/form-data" onsubmit="return false"  id="form">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="col-sm-6">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="soft_order_num" value="{{$data['order_number']}}">
                    <div class="form-group">
                        <label for="firstname" class="col-sm-4 control-label">姓名</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="name"
                                   placeholder=""><span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-4 control-label">身份证号</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="id_card"
                                   placeholder=""><span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-4 control-label">手机号码</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="phone"
                                   placeholder="" value="{{$data['cellphone_number']}}"><span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-4 control-label">邮箱</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="email"
                                   placeholder="" value="{{$data['email']}}"><span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-4 control-label">单位名称</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="school" placeholder=""><span style="color: red;float: right;margin-top: -25px;margin-right: -20px">*</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain_1" class="col-sm-4 control-label">5个备用的3级域名</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" id="domain_1" class="form-control do" placeholder="" name="domain_1" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">.partner.onmschina.cn</span><span style="color: red;float: right;margin-right: -20px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain_2" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" id="domain_2" class="form-control do" placeholder="" name="domain_2" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">.partner.onmschina.cn</span><span style="color: red;float: right;margin-right: -20px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain_3" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" id="domain_3" class="form-control do" placeholder="" name="domain_3" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">.partner.onmschina.cn</span><span style="color: red;float: right;margin-right: -20px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain_4" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" id="domain_4" class="form-control do" placeholder="" name="domain_4" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">.partner.onmschina.cn</span><span style="color: red;float: right;margin-right: -20px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain_5" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" id="domain_5" class="form-control do" placeholder="" name="domain_5" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">.partner.onmschina.cn</span><span style="color: red;float: right;margin-right: -20px">*</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6" style="height: 492px;">
                    <div style="margin-top:250px;">
                        <h5 style="line-height: 30px;"><span style="color: red;">*</span> 您需要用户 ID 和密码来登录帐户，我们会从您的备用域名中选择一个来帮您完成注册，完成注册后，您可以修改登陆密码。</h5>
                        <h4>示例：</h4>
                        <p>“清华大学” 的域名推荐为：</p>
                        <p>qinghuadaxue.partner.onmschina.cn</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="firstname" class="col-sm-2 control-label">身份证照片</label>
                    <div class="col-sm-10"></div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">
                    </div>

                    <div class="col-sm-10">
                        <div class="col-md-6">



                            {{--身份证照片--}}
                            <input type="file" name="pic_one" onchange="preview(this,'one')">
                            <div  id="one" class="row" style="width: 375px; height: 240px;  border: 2px solid #cccccc; margin: 10px 0;">
                                <div style="margin-top: 100px;margin-left: 124px;color: red">上传身份证正面照</div>
                            </div>
                            <input type="file" name="pic_two" onchange="preview(this,'two')">
                            <div id="two" class="row" style="width: 375px; height: 240px;  border: 2px solid #cccccc; margin: 10px 0;">
                                <div style="margin-top: 100px;margin-left: 124px;color: red">上传身份证背面照</div>
                            </div>



                        </div>
                        <div class="col-md-6" >
                            <div class="row" style="margin-top: 10px;">
                                <div style="margin-left: 10px;margin-right: 5px">
                                    <p style="font-weight: bolder;">图例及要求</p>
                                    <p>每张图片不得超过 <span STYLE="color: red;font-weight: bold;font-size: larger">2M</span>，支持JPG，JPEG，PNG三类图片类型格式；调整好光纤，文字清晰可见证件:显示完整，占据图像面基80%以上。</p>
                                    <img src="/images/1.jpg" width="304"  style="margin:5px auto">
                                    <img src="/images/2.jpg"  width="304"  style="margin:5px auto">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <button class="btn btn-primary" type="button" style="display: table;margin:10px auto; padding: 15px; width: 300px;" id="sub">提交</button>
    </form>
    <br>
    <div style="clear: both;"></div><br>
    <div style="height: 50px; width: 100%; border-top: 1px solid #cccccc; padding-top: 20px; font-size: 12px; color: #999999; line-height: 	15px;">
        <p>京ICP备12014130号-1 © 2005-2018 版权所有，并保留所有权利</p>
        <p>支持部门: 中华人民共和国教育部科技发展中心</p>
        <p>服务提供商: 赛尔网络 CERNET</p>
        <p><a href="./ace.jpg" target="_blank">微软ACE资质证书</a></p>
    </div>
</div>
</body>
</html>
<script type="text/javascript">
    function preview(file,id) {
        var prevDiv = document.getElementById(id);
        if (file.files && file.files[0]) {
            var reader = new FileReader();
            reader.onload = function(evt) {
                prevDiv.innerHTML = '<img class="pic" src="' + evt.target.result + '" />';
                prevDiv.style.display = 'block';
            }
            reader.readAsDataURL(file.files[0]);
        } else {
            prevDiv.innerHTML = '<div  style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\'"></div>';
        }
    }
    $('#sub').bind('click',function () {
        var re = checkempty();
        if(!re && re != undefined) return false;
        $(this).unbind('click');
        var data = new FormData($('#form')[0]);
        $.ajax({
            url: '/third',
            type: 'POST',
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#sub").html("正在上传...");
                $("#sub").attr("class",'btn-danger');
                $('#sub').attr('disable','disable');

                layer.load(0, {
                    shade: [0.7,'#fff'] //0.1透明度的白色背景
                });
            },
            complete: function () {
//                layer.closeAll();
//                window.location.href='/third';
            }
        }).done(function (e) {
            if(e == 0){
                layer.alert('请上传png/jpg/gif格式的图片！');
                return false;
            }else if(e == -1){
                layer.alert('提交失败！');
                return false;
            }else if(e == 3){
                layer.alert('请上传小于2M的图片!');
                return false;
            }else{
                layer.alert('提交成功！', {
                    icon:6,
                    closeBtn:0,
                    title:false
                },function () {
//                    window.location.href='/last/'+e+'?s=done';
                });
            }
        });
    });

    
    function checkempty() {
        var pic_one = $("input[name='pic_one']").val();
        if(pic_one != undefined && pic_one == ''){
            layer.alert('请上传正面身份证照片!');
            return false;
        }
        var pic_two = $("input[name='pic_two']").val();
        if(pic_two != undefined && pic_two == ''){
            layer.alert('请上传背面身份证照片!');
            return false;
        }
        var name = $("input[name='name']").val();
        if(name != undefined && name == ''){
            layer.alert('请填写姓名');
            return false;
        }
        var id_card = $("input[name='id_card']").val();
        var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        if(id_card == '' || !reg.test(id_card)){
            layer.alert('请填写正确的身份证号码!');
            return false;
        }
        var phone = $("input[name='phone']").val();
        var reg = /^1(3|4|5|7|8)\d{9}$/;
        if(phone == '' || !reg.test(phone)){
            layer.alert('请填写正确的手机号码!');
            return false;
        }
        var email = $("input[name='email']").val();
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
        if(email == '' || !reg.test(email)){
            layer.alert('请填写正确的邮箱地址!');
            return false;
        }
        var school = $("input[name='school']").val();
        if(school == '' ){
            layer.alert('请填写单位名称!');
            return false;
        }
        var num = 0;
        $('.do').each(function(k,v){
            if($(this).val() != ''){
                num++;
            }
        });
        if(num != 5){
            layer.alert('请填写5个备用的3级域名!');
            return false;
        }

    }



</script>