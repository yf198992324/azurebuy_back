<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>AZURE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">
            <h2>确认消息及付款</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <p style="margin: 10px 1px"><strong style="font-size: 18px;">个人及单位信息</strong></p>

            <table class="table table-striped table-bordered table-hover">
                <input type="hidden" value="{{$info->id}}" name="userid">
                <input type="hidden" value="{{$info->soft_order_num}}" name="soft_order_num">
                <tbody>

                <tr>
                    <td>姓名</td>
                    <td>{{$info->name}}</td>
                </tr>

                <tr>
                    <td>单位名称</td>
                    <td>{{$info->school}}</td>
                </tr>
                <tr>
                    <td>电话</td>
                    <td>{{$info->phone}}</td>
                </tr>

                <tr>
                    <td>邮箱</td>
                    <td>{{$info->email}}</td>
                </tr>

                </tbody>
            </table>

        </div>
    </div>
    <style type="text/css">
        table.selftable{
            border-collapse: collapse;
            border: 1px solid #999;
        }

        table.selftable td {
            border-top: 0;
            border-right: 1px solid #999;
            border-bottom: 1px solid #999;
            border-left: 0;
            width: 100px;
            text-align: center;
            padding: 5px;
            vertical-align: top;
        }

        table.selftable tr td p {
            margin-top: 10px;
        }
    </style>
    <br>
    <p style="margin: 10px 1px"><strong style="font-size: 18px;"> 提示信息</strong></p>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div>系统已记录您的信息，我们的客服将会在48个工时内与您联系，帮助您完成购买操作。</div>
            <div>您也可以拨打我们的客服电话进行咨询，客服电话：400-686-9667</div>
            <a href="https://www.mspil.cn/#/cloud">返回微软教育 &gt;&gt;</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div><span style="color:red">*</span>本页面不可刷新，刷新后，页面数据将可能失效！</div>
        </div>
    </div>

    <div style="clear: both;"></div><br>
    <div style="height: 50px; width: 100%; border-top: 1px solid #cccccc; padding-top: 20px; font-size: 12px; color: #999999; line-height: 	15px;">
        <p>京ICP备12014130号-1 © 2005-2018 版权所有，并保留所有权利</p>
        <p>支持部门: 中华人民共和国教育部科技发展中心</p>
        <p>服务提供商: 赛尔网络 CERNET</p>
        <p><a href="/images/ace.jpg" target="_blank">微软ACE资质证书</a></p>
    </div>
</div>
</body>
</html>