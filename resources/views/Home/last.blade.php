<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>AZURE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">
            <h2>确认消息及付款</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <p style="margin: 10px 1px"><strong style="font-size: 18px;">个人及单位信息</strong></p>

            <table class="table table-striped table-bordered table-hover">
                <input type="hidden" value="{{$info->id}}" name="userid">
                <input type="hidden" value="{{$info->soft_order_num}}" name="soft_order_num">
                <tbody>

                <tr>
                    <td>姓名</td>
                    <td>{{$info->name}}</td>
                </tr>

                <tr>
                    <td>单位名称</td>
                    <td>{{$info->school}}</td>
                </tr>
                <tr>
                    <td>电话</td>
                    <td>{{$info->phone}}</td>
                </tr>

                <tr>
                    <td>邮箱</td>
                    <td>{{$info->email}}</td>
                </tr>

<!--                 <tr>
                    <td rowspan=5>申请域名</td>
                    <td>{{$info->domain}}</td>
                </tr>
 -->
                </tbody>
            </table>

        </div>
    </div>
    {{--<p style="margin: 10px 1px"><strong style="font-size: 18px;">您的套包包括以下是产品</strong></p>--}}
    <style type="text/css">
        table.selftable{
            border-collapse: collapse;
            border: 1px solid #999;
        }

        table.selftable td {
            border-top: 0;
            border-right: 1px solid #999;
            border-bottom: 1px solid #999;
            border-left: 0;
            width: 100px;
            text-align: center;
            padding: 5px;
            vertical-align: top;
        }

        table.selftable tr td p {
            margin-top: 10px;
        }
    </style>
    <br>
    <p style="margin: 10px 1px"><strong style="font-size: 18px;"> 选择支付方式</strong></p>
    <div class="row">
        <div class="col-md-4">
            <div class="checkbox">
                <label>
                    <input type="radio" style="margin-top: 20px" name='pay' checked value="1"><img src="/images/z.jpg" width="70" > 支付宝支付
                </label>
            </div>
        </div>

        <div class="col-md-4">
            <div class="checkbox">
                <label>
                    <input type="radio" style="margin-top: 20px" name='pay' value="2"><img src="/images/y.jpg" width="65"> 网银在线
                </label>
            </div>
        </div>

        {{--<div class="col-md-4">--}}
            {{--<div class="checkbox">--}}
                {{--<label>--}}
                    {{--<input type="radio" style="margin-top: 20px" name='pay' value="3"><img src="/images/g.jpg" width="65"> 转账--}}
                {{--</label>--}}
            {{--</div>--}}
        {{--</div>--}}

    </div>
    <label for="lastname" class="col-sm-2 control-label">请输入充值金额:</label>
    <div class="col-sm-4">
        <input type="number" id="num" class="form-control" onblur="isNumberBy100(this)" placeholder="请输入充值金额,1000以上，100的整数倍">
    </div>
    <p style="width: 100%; text-align: right; line-height: 30px;" id="price">
        应付：¥ 0 元
    </p>
    <button  class="btn btn-primary" style="float: right;" id="topay">确认支付</button>
    <br>
    <br>
    <p style="width: 100%; text-align: right; line-height: 30px; color: red;">
        在您支付完成后，我们的客服将会在3个工作日内，与您联系，帮助您完成购买操作。	客服电话为：400-686-9667 周一至周五(节假日除外) 9:00-12:00,13:00-17:15
    </p>

    <div style="clear: both;"></div><br>
    <div style="height: 50px; width: 100%; border-top: 1px solid #cccccc; padding-top: 20px; font-size: 12px; color: #999999; line-height: 	15px;">
        <p>京ICP备12014130号-1 © 2005-2019 版权所有，并保留所有权利</p>
        <!-- <p>支持部门: 中华人民共和国教育部科技发展中心</p> -->
        <!-- <p>服务提供商: 赛尔网络 CERNET</p> -->
        <p><a href="/images/ace2.png" target="_blank">微软AEP资质证书</a></p>
    </div>
</div>
</body>
</html>
<script>

    function isNumberBy100(obj) {
        var num = $(obj).val();
        var re = /^[0-9]*[0-9]$/i;       //校验是否为数字
        // if(re.test(num) && num%100===0 && num >= 1000) {
       if(num) {
            var all = parseInt(num) + parseInt(num) * 0.1;

           var s = '<span>应付：¥'+num+'.00 + ¥'+num+'.00 * 10% = ¥'+all+'.00</span>';
           $('#price').html(s);
            return false;
        }else {
            $(obj).val('');
            var s = '<span>应付：¥ 0</span>';
            $('#price').html(s);
            alert('请输入1000以上，100的整数倍金额！');
            return false;
        }
    }

    $('#topay').click(function () {
        var re = /^[0-9]*[0-9]$/i;
        var num = $('#num').val();
        var soft_order_num = $("input[name='soft_order_num']").val();
        var userid = $("input[name='userid']").val();
        var payway = $('input:radio:checked').val();
//        alert(payway);
       if(num == ''){
        alert('请输入金额!');
            return false;
       }
        if( soft_order_num == '' || userid == '' || payway == ''){
            alert('参数错误，请返回重试!');
            return false;
        }

        // if(num > 5000)
        // {

        // }
        // var mymessage=confirm("你喜欢JavaScript吗?");
        // if(mymessage==true)
        // {
        //     document.write("很好,加油!");
        // }
        // else
        // {
        //     document.write("JS功能强大，要学习噢!");
        // }

        // if(re.test(num) && num%100===0 && num >= 1000) {
       if(num){
            if(num > 5000)
            {
                if(confirm("您购买的金额较大，您是否需要线下购买？"))
                {
                    window.location.href='/contact';
                }else{
                    $.post('/do_order',{num:num,soft_order_num:soft_order_num,userid:userid,payway:payway,'_token':'{{csrf_token()}}'},function (e) {
                        if(e.status == 1){
                            window.location.href='/topay/'+e.orderid;
                        }else if(e.status == -1){
                            alert('订单同步错误!');
                            return false;
                        }else if(e.status == -2){
                            alert('订单已经支付!');
                            return false;
                        }else{
                            alert('参数错误，请返回重试！');
                            window.location.href='/error';

                        }
                    },'json');
                }
            }else{
                $.post('/do_order',{num:num,soft_order_num:soft_order_num,userid:userid,payway:payway,'_token':'{{csrf_token()}}'},function (e) {
                    if(e.status == 1){
                        window.location.href='/topay/'+e.orderid;
                    }else if(e.status == -1){
                        alert('订单同步错误!');
                        return false;
                    }else if(e.status == -2){
                        alert('订单已经支付!');
                        return false;
                    }else{
                        alert('参数错误，请返回重试！');
                        window.location.href='/error';

                    }
                },'json');
            }
        }else{
            alert('请输入1000以上，100的整数倍金额！');
            return false;
        }
    });


</script>