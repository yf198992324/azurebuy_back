<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>test</title>
	<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class ="container">
	<div class="row">
	<h1><strong>用户认证信息</strong></h1></div>

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12" style="box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;">
			<form class="form-horizontal" role="form" style="margin-top: 10px" id="data">
				<div class="form-group">
					<label for="firstname" class="col-sm-1 control-label">姓名</label>
					<div class="col-sm-3">
						<input  class="form-control" name="name"
					   placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label for="firstname" class="col-sm-1 control-label">手机号</label>
					<div class="col-sm-3">
						<input  class="form-control" name="phone"
					   placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label for="firstname" class="col-sm-1 control-label">身份证号</label>
					<div class="col-sm-3">
						<input  class="form-control" name="id_card"
					   placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label for="firstname" class="col-sm-1 control-label">邮箱</label>
					<div class="col-sm-3">
						<input  class="form-control" name="email"
					   placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label for="firstname" class="col-sm-1 control-label">组织名称</label>
					<div class="col-sm-3">
						<input  class="form-control" name="school"
					   placeholder="">
					</div>
				</div>
				<div class="form-group">
					<p class="col-sm-4">5个备用的3级域名</p>
				</div>
				<div class="form-group">
					<label for="firstname" class="col-sm-1"></label>
					<div class="col-sm-8">
						<input class="col-sm-3" name="domain_1"
					   placeholder="">
					<span class="col-sm-3">.partner.onmschina.cn</span>
					</div>
				</div>
				<div class="form-group">
					<label for="firstname" class="col-sm-1"></label>
					<div class="col-sm-8">
						<input class="col-sm-3" name="domain_2"
					   placeholder="">
					<span class="col-sm-3">.partner.onmschina.cn</span>
					</div>
				</div>
				<div class="form-group">
					<label for="firstname" class="col-sm-1"></label>
					<div class="col-sm-8">
						<input class="col-sm-3" name="domain_3"
					   placeholder="">
					<span class="col-sm-3">.partner.onmschina.cn</span>
					</div>
				</div>
				<div class="form-group">
					<label for="firstname" class="col-sm-1"></label>
					<div class="col-sm-8">
						<input class="col-sm-3" name="domain_4"
					   placeholder="">
					<span class="col-sm-3">.partner.onmschina.cn</span>
					</div>
				</div>
				<div class="form-group">
					<label for="firstname" class="col-sm-1"></label>
					<div class="col-sm-8">
						<input class="col-sm-3" name="domain_5"
					   placeholder="">
					<span class="col-sm-3">.partner.onmschina.cn</span>
					</div>
				</div>

				<div class="form-group">
					<p class="col-sm-4">身份证照片</p>
				</div>

				<div class="form-group">
						<div class="col-sm-1">
						</div>

						<div class="col-sm-10">
							<div class="col-md-5">
								<div class="row" style="border: 2px solid black;margin-top: 2px;margin-bottom: 20px">
									<div style="margin: 120px 30px">
										<h3>上传身份证正面照片</h3>

										<input type="file" name="pic_one">
									</div>

								</div>

								<div class="row" style="border: 2px solid black">
									<div style="margin: 120px 30px">
										<h3>上传身份证正面照片</h3>

										<input type="file" name="pic_two">
									</div>
								</div>
							</div>

							<div class="col-md-1">
							</div>

							<div class="col-md-5" >
								<div class="row" style="margin-top: 2px;border: 2px solid black">
								<div style="margin-left: 10px;margin-right: 5px">
									<h3>图例及要求</h3>
									<p>每张图片不得超过4M，支持JPG，JPEG，PNG三类图片类型格式；调整好光纤，文字清晰可见证件:显示完整，占据图像面基80%以上。</p>
										<img src="/images/1.jpg" width="304"  style="margin:5px auto">
										<img src="/images/2.jpg"  width="304"  style="margin:5px auto">
								</div>

								</div>
							</div>

						</div>

						<div class="col-sm-1">
						</div>

				</div>

			</form>
		</div>
	</div>
			<a href="/last" class="btn btn-primary" style="display: table;margin:10px auto">提交</a>

</div>

</body>
</html>
<script>

</script>