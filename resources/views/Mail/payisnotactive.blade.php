<!DOCTYPE html>
            <html>
                <head>
                    <title>Laravel</title>
                </head>
                <body>
                    <p style="text-align: left;">尊敬的微软教育用户：</p>
                    <div style="margin-left: 30px;">
                        <p>感谢您对微软教育的关注与支持！</p>

                        <p>我们已经收到的购买申请，您的充值金额为{{$order->total_price}}元，您可以获得的Azure 资源为{{$order->account_balance}}元</p>
                        <br>
                        <hr> 
                        <br>
                        <p>我们接下来会通过电话联系您为您开通Azure 资源，资源开通后有效期为12个月，请留意接听客服电话!</p>
                        <br>
                        <p>在此, 为了帮助您更好的了解Azure的相关技术资源，给您提供以下学习微软Azure的资源网站，希望对您有帮助</p>
                        <p>1. 微软教育mspil网站视频学习地址:https://www.mspil.cn/#/lesson/9</p>
                        <p>2. Azure直播课堂: https://www.azure.cn/zh-cn/webinars/</p>
                        <p>3. Azure上云须知: https://www.azure.cn/zh-cn/webinars/ </p>
                        <p>4. Azure简介: https://docs.microsoft.com/zh-cn/learn/azure/</p>
                        <br>
                        <p>如果您有任何问题，可以直接回复邮件，我们会尽快答复您！</p>
                        <br>
                        <p>同时，您可以通过关注微软教育公众号和微软学生汇公众号了解更多关于微软教育的解决方案以及客户案例，期待您的关注!</p>
                        <br>
                        <p>
                            <img src="http://azure.shop.edu.cn/images/weix1.png">
                        </p>
                    </div>
                </body>
            </html>