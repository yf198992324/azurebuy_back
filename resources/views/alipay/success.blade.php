<!DOCTYPE html>
<html>
    <head>
        <title>AZURE</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }
        </style>
    </head>
    <body>
        <div class="jumbotron" style="margin-top: 300px;">
            <div class="container">
                <h1>支付成功！</h1>
                <p>我们已经发送一封关于支付状态的邮件到您的预留邮箱，请查收，感谢您的购买！</p>
                <p><a href="https://www.mspil.cn/#/cloud" class="btn btn-primary btn-lg" href="#" role="button">返回微软教育 &gt;&gt;</a></p>
            </div>
        </div>
    </body>
</html>
