<!DOCTYPE html>
<html>
    <head>
        <title>AZURE</title>

        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                /*color: red;*/
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

/*            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }*/

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="jumbotron" style="margin-top: 300px;">
            <div class="container">
                <h1>支付失败！</h1>
                <p><a href="https://www.mspil.cn/#/cloud" class="btn btn-primary btn-lg" href="#" role="button">返回微软教育 &gt;&gt;</a></p>
            </div>
        </div>
    </body>
</html>
