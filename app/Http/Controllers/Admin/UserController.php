<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function index()
    {
        $input = Input::all();
        $users = User::getUserList($input);
//        dd($users);
        return view('Admin.user',['users'=>$users,'input'=>$input]);
    }

    public function pass()
    {
        $input = Input::all();
        $res = DB::table('user_info')->where('id',$input['id'])->update(['status'=>1]);
        return $res ? 1 : 0 ;
    }
    public function noPass()
    {
        $input = Input::all();
        $res = DB::table('user_info')->where('id',$input['id'])->update(['status'=>2]);
        return $res ? 1 : 0 ;
    }

    public function userInfo()
    {
        return view('Admin.userInfo');
    }
    public function changePwd()
    {
        return view('Admin.changePwd');
    }

    public function downloadPic()
    {
        $input = Input::all();
        $files = explode(',',$input['file']);
        $zip=new \ZipArchive();
        $zipname = public_path().'/pic.zip';
        @unlink($zipname);
        if($zip->open( $zipname,\ZipArchive::CREATE)===TRUE){
            $zip->addEmptyDir('pic');
            foreach ($files as $k => $v){
                $filename = public_path().parse_url($v)['path'];
                if(!file_exists($filename)) return '源图片不存在，请刷新手动保存!';
                $fileData = file_get_contents( $filename);
                if ($fileData) {
                    $zip->addFromString('pic/'.$k.'.jpg', $fileData);
                }
            }
            $zip->close();
        }
        $this->download($zipname);
    }

    private function download($item)
    {
        header('content-disposition:attachment;filename='. basename($item));
        header('content-length:'. filesize($item));
        readfile($item);
    }


    public function checkpass()
    {
         $input = Input::all();
         $user = Auth::user();
        $res = \Hash::check($input['pass'],$user->password);
        return $res ? 1 : 0;
    }
    public function changepass()
    {
         $input = Input::all();
         $res = DB::table('user')->where('userid',Auth::user()->userid)->update(['password'=>bcrypt(trim($input['pass']))]);
         return $res ? 1 : 0;
    }

    public function changeEmail()
    {
        $input = Input::all();
        $res = DB::table('user_info')->where(['id'=>$input['uid']])->update(['email'=>$input['new_email']]);
        if($res){
            echo '<script>alert("修改邮箱成功!");window.location.href="/index";</script>';
        }else{
            echo  '<script>alert("修改邮箱失败!");window.location.href="/index";</script>';
        }
    }


}