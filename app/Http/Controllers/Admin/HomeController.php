<?php

/**
 *  AZURE  管理后台
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{

    public function Login()
    {
        $input = Input::all();
        if(Auth::check())
            return redirect('/index');
        return view('Admin.login',$input);
    }

    public function postLogin( Request $request )
    {
        $all = $request->all();
        $res = Auth::attempt(['name'=>trim($all['username']),'password'=>trim($all['password'])]);
        if($res){
            return redirect('/index'); // 登录成功
        }else{
            //登录失败
            return redirect('/admin?status=-1');
        }
    }
    public function Index()
    {
        return view('Admin.index');
    }
    public function Main()
    {
        return view('Admin.main');
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->intended('/index');
    }

    /**
     * 首页获取用户数和订单数
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNum()
    {
        $or = DB::table('orders')->count(); // 订单数
        $us = DB::table('user_info')->count(); // 用户数
        return response()->json(['o'=>$or,'u'=>$us]);
    }

}