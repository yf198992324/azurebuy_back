<?php

namespace App\Http\Controllers\Admin;


use App\Common;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    public function index()
    {
        $input = Input::all();
        $orders = User::getOrderList($input);
        $payway = DB::table('pay_way')->lists('name','id');
        return view('Admin.order',['orders'=>$orders,'input'=>$input,'payway'=>$payway]);
    }

    public function registlist()
    {
        $input = Input::all();
        $orders = User::getOrderList($input,false,1); // 支付成功再注册
        $payway = DB::table('pay_way')->lists('name','id');
        return view('Admin.registlist',['orders'=>$orders,'input'=>$input,'payway'=>$payway]);
    }
    public function paylist()
    {
        $input = Input::all();
        $orders = User::getOrderList($input,false,4); // 注册成功再充值
        $payway = DB::table('pay_way')->lists('name','id');
        return view('Admin.paylist',['orders'=>$orders,'input'=>$input,'payway'=>$payway]);
    }
    public function confirmlist()
    {
        $input = Input::all();
        $orders = User::getConfirmList($input); // 充值成功再确认
        $payway = DB::table('pay_way')->lists('name','id');
//        dd($orders);
        return view('Admin.confirmlist',['orders'=>$orders,'input'=>$input,'payway'=>$payway]);
    }

    public function toconfirm()
    {
        $input = Input::all();
        $log['uid'] = $input['uid'];
        $log['oid'] = $input['oid'];
        $log['type'] = 3; //确认
        $time = date('Ymdhis');
        $res = DB::table('orders')->where(['user_id'=>$input['uid'],'id'=>$input['oid'],'pay_status'=>5])->update(['pay_status'=>6]);
        if($res){
            $status = 1 ;
            $log['msg'] = '人工确认成功,'.serialize($input).'时间:'.$time;
        }else{
            $status = 0 ;
            $log['msg'] = '人工确认失败,'.serialize($input).'时间:'.$time;
        }
        Common::insertLog($log);
        self::setLog(json_encode($log).'时间----'.$time);
        return $status;
    }

    public function confirmMail()
    {
        $input = Input::all();
        $log['uid'] = $input['uid'];
        $log['oid'] = $input['oid'];
        $log['type'] = 4; //确认
        $order = DB::table('orders')->find($input['oid']);
        $info = DB::table('user_order_acount')->select(DB::raw('user_order_acount.* ,user_info.name,user_info.email'))
            ->leftjoin('user_info', 'user_info.id','=','user_order_acount.user_info_id')
            ->where('user_info.id',$input['uid'])->first();
        $info->id_num = $order->soft_order_num;
        $info->pay_time = $order->pay_time;
        $log['msg'] = '人工确认邮件--'.serialize($input);
        Common::insertLog($log);
        $res = Common::sendMail($info,3,$info->money);
        return $res ? 1 : 0;
    }

    public function toregist()
    {
        $input = Input::all();
        if($input['acount'] == '' || $input['pass'] == ''){
            echo '<script>alert("账号密码不能为空,请重新输入!");window.location.href="/index";</script>';
        }else{
            $log['uid'] = $input['uid'];
            $log['oid'] = $input['oid'];
            $log['type'] = 1;
            $info = DB::table('user_order_acount')->where(['user_info_id'=>$input['uid']])->first();
            if($info){
                DB::table('orders')->where(['user_info_id'=>$input['uid']])->update(['pay_status'=>4]);
                $log['msg'] = '用户已注册，直接更改状态';
                Common::insertLog($log);
                self::setLog(json_encode($log).'时间----'.date('Ymdhis'));
                echo '<script>alert("该用户已经注册！");window.location.href="/index";</script>';
            }else{
                $data['userid'] = Auth::user()->userid;
                $data['user_info_id'] = $input['uid'];
                $data['user_acount'] = $input['acount'];
                $data['user_pass'] = $input['pass'];
                $data['money'] = 0;
                $res = DB::table('user_order_acount')->insert($data);
                if($res){
                    DB::table('orders')->where(['id'=>$input['oid']])->update(['pay_status'=>4]);
                    $log['msg'] = '用户第一次注册，更改状态';
                    Common::insertLog($log);
                    self::setLog(json_encode($log).'时间----'.date('Ymdhis'));
                    echo '<script>alert("注册成功!");window.location.href="/index";</script>';
                }else{
                    $log['msg'] = '日志写入错误';
                    Common::insertLog($log);
                    self::setLog(json_encode($log).'时间----'.date('Ymdhis'));
                    echo '<script>alert("注册失败!");window.location.href="/index";</script>';
                }
            }
        }
    }

    public function torecharge()
    {
        $input = Input::all();
        $log['uid'] = $input['uid'];
        $log['oid'] = $input['oid'];
        $log['type'] = 2;
        $order = DB::table('orders')->where('id',$input['oid'])->first();

        $status = 1 ; // 默认充值成功
        $msg = '<script>alert("充值成功!");window.location.href="/index";</script>';

        if($order->account_balance != $input['account_balance'] || $order->account_balance < $input['recharge_money']){
            $log['msg'] = '订单余额不足，或充值金额大于余额';
            $status = 0 ;
            $msg = '<script>alert("订单余额不足，或充值金额大于余额");window.location.href="/index";</script>';
        }else{
            if($order->account_balance == $input['recharge_money']){
                /*充值完，更新状态为已充值状态*/
                DB::table('orders')->where('id',$input['oid'])->update(['pay_status'=>5]);
            }
            try{
                DB::transaction( function () use ($input) {
                    DB::table('orders')->where('id',$input['oid'])->decrement('account_balance', $input['recharge_money']);
                    DB::table('user_order_acount')->where('user_info_id',$input['uid'])->increment('money',$input['recharge_money']);
                    session(['ok'=>1]);
                });
            }catch(\Exception $ex){
                $log['msg'] = '订单充值失败';
                $status = 0;
                $msg = '<script>alert("订单充值失败");window.location.href="/index";</script>';
            }
        }
        if(session('ok')){ // 事务成功
            DB::table('recharge_log')->insert(['userid'=>Auth::user()->userid,'user_info_id'=>$input['uid'],'oid'=>$input['oid'],'money'=>$input['recharge_money'],'status'=>$status]);
            $log['msg'] = '订单充值成功，充值金额'.$input['recharge_money'];
            /*充值成功，发送邮件*/
            $info = DB::table('user_order_acount')->select(DB::raw('user_order_acount.* ,user_info.name,user_info.email'))
                ->leftjoin('user_info', 'user_info.id','=','user_order_acount.user_info_id')
                ->where('user_info.id',$input['uid'])->first();
            $info->id_num = $order->soft_order_num;
            $info->pay_time = $order->pay_time;
            Common::insertLog($log);
            Common::sendMail($info,3,$info->money);
            $log['msg'] = '注册充值成功邮件--'.serialize($input);
            $log['type'] = 4;
            Common::insertLog($log);
        }
        Common::insertLog($log);
        self::setLog(json_encode($log).'时间----'.date('Ymdhis'));
        echo $msg;
    }


    public function acountlist()
    {
        $input = Input::all();
        $orders = DB::table('user_order_acount')
            ->select(DB::raw('user_order_acount.*,user_info.name,user_info.phone,user_info.email'))
            ->leftjoin('user_info','user_info.id','=','user_order_acount.user_info_id')
            ->where('user_order_acount.status',1)->orderby('user_order_acount.created_at','desc')->paginate(10);
//        dd($orders);
        return view('Admin.acountlist',['orders'=>$orders,'input'=>$input]);
    }
    
    
    
}