<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{

    public function downloadUser()
    {
        $input = Input::all();
        $users = User::getUserList($input,true);
        $data = [];
        foreach ($users as $k => $v){
            $data[$k][] = $v->name;
            $data[$k][] = $v->email;
            $data[$k][] = $v->phone;
            $data[$k][] = $v->id_card.' ';
            $data[$k][] = $v->school;
            $data[$k][] = $v->soft_order_num;
            $data[$k][] = $v->id_card_picurl;
            $data[$k][] = $v->domain;
            $data[$k][] = $v->created_at;
            $data[$k][] = $v->status ? '已通过':'待审核';
        }
        $head = ['姓名','邮箱','手机号','身份证号','单位','微软同步号','身份证在线地址','域名','创建时间','状态'];
        array_unshift($data, $head);
        Excel::create('user_'.date('YmdHis'),function ($excel) use ($data) {
            $excel->sheet('user',function ($sheet) use ($data) {
                $sheet->rows($data);
            });
        })->export('xls');
    }



    public function downloadOrder()
    {
        $input = Input::all();
        $orders = User::getOrderList($input,true);
        $payway = DB::table('pay_way')->lists('name','id');
        $data = [];
        foreach ($orders as $k => $v){
            $data[$k][] = $v->name;
            $data[$k][] = $v->school;
            $data[$k][] = $v->phone;
            $data[$k][] = (string)$v->id_card.' ';
            $data[$k][] = $v->soft_order_num;
            $data[$k][] = $payway[$v->pay_way_id];
            $data[$k][] = $v->order_num;
            $data[$k][] = $v->total_price;
            $data[$k][] = $v->price;
            $data[$k][] = $v->poundage;
            $data[$k][] = $v->created_at;
            $data[$k][] = $v->pay_time;
            $data[$k][] = $v->pay_status ? '已支付':'未支付';
        }
        $head = ['姓名','单位','手机号','身份证号','微软同步号','支付方式','订单号','总金额','金额','手续费','创建时间','支付时间','支付状态'];
        array_unshift($data, $head);
        Excel::create('order_'.date('YmdHis'),function ($excel) use ($data) {
            $excel->sheet('order',function ($sheet) use ($data) {
                $sheet->rows($data);
            });
        })->export('xls');
    }

}