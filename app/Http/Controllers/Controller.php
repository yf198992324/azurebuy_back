<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;


    /**
     * 日志记录
     * @param $data
     */
    public static function setLog($data){
        $logfilename=date('Y-m-d');
        file_put_contents(storage_path()."/logs/$logfilename.txt",$data.PHP_EOL, FILE_APPEND);
    }
}
