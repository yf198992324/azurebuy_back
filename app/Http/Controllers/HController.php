<?php

namespace App\Http\Controllers;


class HController extends Controller
{
    public function index()
    {
        if(!isset($_GET['a'])){
            $data = [];
        }else{
            $get = $_GET['a'];
            if($get == 0 || !is_numeric($get)){
                $data['msg'] = '输入的内容错误!';
                $data['data'] = [];
            }else{
                $data['msg']='生成的号码';
                for ($i=1;$i<=$get;$i++){
                    if($i == 20) break;
                    $data['data'][]=$this->foo($get);
                }
            }
        }
        return view('H.ssq',['data'=>$data]);
    }

    public function getSsq()
    {
        $get = isset($_GET['a']) ?  (int)$_GET['a'] : 0;
        if($get == 0 || !is_numeric($get)){
            return 0;
        }
        return json_encode($this->foo($get));
    }

    public function foo ( ){
        $result = [];
            $red = range(1,33);
            shuffle($red);

            $i = 0;
            while ($i<6) {
                $lenth = count($red);
                $num = mt_rand(0,$lenth-1);
                $result[] = $red[$num];
                unset($red[$num]);
                shuffle($red);
                $i++;
            }
            sort($result);

            $blue = range(1,16);
            shuffle($blue);
            $result[] = $blue[mt_rand(0,15)];

        return $result;
    }


}