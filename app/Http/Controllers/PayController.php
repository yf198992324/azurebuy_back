<?php

namespace App\Http\Controllers;


use App\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class PayController extends Controller
{

    public function doOrder(Request $request)
    {
        $all = $request->all();
        $info = Common::getUserInfo($all['userid']);

        if(!$info || $info->soft_order_num != $all['soft_order_num']){
            return json_encode(['status'=>0]);
        }
        $orders = DB::table('orders')->where(['soft_order_num'=>$all['soft_order_num']])->first();
        if($orders){
            $res = $orders->pay_status ? -1 : $orders->id;
            $price = $orders->total_price;
            if($all['payway'] != $orders->pay_way_id && $orders->pay_status == 0){
                DB::table('orders')->where(['soft_order_num'=>$all['soft_order_num']])->update(['pay_way_id' =>$all['payway']]);
            }
            if($price != $all['num']){
                $newprice = $all['num']; // 支付金额
                $order['total_price'] = $newprice + $newprice * 0.1; //总金额
                $order['price'] = $newprice; // 用户输入金额
                $order['account_balance'] = $newprice; // 可充值的金额
                $order['poundage'] = $newprice * 0.1; // 手续费
                DB::table('orders')->where(['soft_order_num'=>$all['soft_order_num']])->update($order);
            }
        }else{
            $payway = $all['payway']; //支付方式
            $price = $all['num']; // 支付金额
            $order['order_num'] = 'AP' . time() . rand(1000,9999);
            $order['user_id'] = $all['userid'];
            $order['pay_way_id'] = $payway;
            $order['total_price'] = $price + $price * 0.1; //总金额
            $order['price'] = $price; // 用户输入金额
            $order['account_balance'] = $price; // 可充值的金额
            $order['poundage'] = $price * 0.1; // 手续费
            $order['soft_order_num'] = $all['soft_order_num'];
            $res = DB::table('orders')->insertGetId($order);
            DB::table('user_info')->where('id',$all['userid'])->increment('money', $price);
        }
        if($res && $res > 0){
            /*同步微软订单*/
            $curl = Common::upMicSoft($info,$price,2);
            self::setLog(json_encode($curl).'   订单号:'.$info->soft_order_num.'--'.date('Y-m-d h:i:s'));
            if($curl['error'] == 0){
                session(['orderid'=>$res]);
                return json_encode(['status'=>1,'orderid'=>$res,'order_number'=>$info->soft_order_num]);
            }else{
                /*订单同步错误*/
                return json_encode(['status'=>-1,'orderid'=>$res,'order_number'=>$info->soft_order_num]);
            }

        }
        return  json_encode(['status'=>-2]);

    }

    public function toPay($orderid)
    {
        $order = Common::getOrderDetailById($orderid);
        if(session('orderid') != $orderid || !$order){
            return json_encode(['status'=>0,'msg'=>'参数错误，请重新购买!'],JSON_UNESCAPED_UNICODE );
        }
        if($order->pay_way_id == 1){
            //支付宝
            $alipay = app('alipay.web');
            $alipay->setOutTradeNo($order->order_num);
            $alipay->setTotalFee($order->total_price);
            $alipay->setSubject('微软云套餐资费');
            $alipay->setBody('微软云套餐资费+赛尔网络代理费');

//            $alipay->setQrPayMode('4'); //该设置为可选，添加该参数设置，支持二维码支付。
            // 跳转到支付页面。
            // echo $alipay->getPayLink();exit;
            return redirect()->to($alipay->getPayLink());
        }
        if($order->pay_way_id == 2){
            // 网银在线
            $url = (new Common)->GetChinaBankUrl($order);
//            dd($url);
            Header("Location: $url");

        }





    }

    /**
     * 异步通知
     */
    public function webNotify()
    {
        $all = Input::all();
        $orderinfo = DB::table('orders')->where('order_num',$all['out_trade_no'])->first();
        $userinfo = DB::table('user_info')->find($orderinfo->user_id);
        // 验证请求。
        if (! app('alipay.web')->verify()) {
            DB::table('orders')->where(['order_num'=>$all['out_trade_no']])->update(['pay_status'=>2,'pay_time'=>$all['notify_time']]);
            $curl = Common::upMicSoft($userinfo,$orderinfo->price,5);
            self::setLog(json_encode($curl).' 支付异步同步失败--  订单号:'.$userinfo->soft_order_num.'--'.date('Y-m-d h:i:s'));
            return 'fail';
        }

        // 判断通知类型。
        switch (Input::get('trade_status')) {
            case 'TRADE_SUCCESS':
            case 'TRADE_FINISHED':
                // TODO: 支付成功，取得订单号进行其它相关操作。
                $det = DB::table('orders')->where(['order_num'=>$all['out_trade_no']])->first();
                if($det->total_price == $all['total_fee'] && $det->pay_status != 1){
                    DB::table('orders')->where(['order_num'=>$all['out_trade_no']])->update(['pay_status'=>1,'pay_time'=>$all['notify_time'],'status'=>1]);
                    $curl = Common::upMicSoft($userinfo,$orderinfo->price,4);
                    self::setLog(json_encode($curl).' 支付异步同步成功--  订单号:'.$userinfo->soft_order_num.'--'.date('Y-m-d h:i:s'));
                }
                break;
        }

        return 'success';
    }

    /*
     * array:18 [▼
  "body" => "goods_description"
  "buyer_email" => "164735040@qq.com"
  "buyer_id" => "2088502719806675"
  "exterface" => "create_direct_pay_by_user"
  "is_success" => "T"
  "notify_id" => "RqPnCoPT3K9%2Fvwbh3InYz5sVge6BBuqi4WmTL5Ru0XcP2opWqkRfFAa%2B8rhkNElUxq%2BP"
  "notify_time" => "2017-06-09 13:57:54"
  "notify_type" => "trade_status_sync"
  "out_trade_no" => "AP14969046211"
  "payment_type" => "1"
  "seller_email" => "jzhj_cw@cernet.com"
  "seller_id" => "2088701785958145"
  "subject" => "goods_name"
  "total_fee" => "0.01"
  "trade_no" => "2017060921001004670288769636"
  "trade_status" => "TRADE_SUCCESS"
  "sign" => "e0607e149017ba311058d9ccef025e3a"
  "sign_type" => "MD5"
]
     */

    /**
     * 同步通知
     */
    public function webReturn(Request $request)
    {
        $all = Input::all();
        $orderinfo = DB::table('orders')->where('order_num',$all['out_trade_no'])->first();
        $userinfo = DB::table('user_info')->find($orderinfo->user_id);
        // 验证请求。
        if (! app('alipay.web')->verify()) {
            $curl= Common::upMicSoft($userinfo,$orderinfo->price,5);
            self::setLog(json_encode($curl).' 支付宝同步同步失败 --  订单号:'.$userinfo->soft_order_num.'--'.date('Y-m-d h:i:s'));
            DB::table('orders')->where(['order_num'=>$all['out_trade_no']])->update(['pay_status'=>2,'pay_time'=>$all['notify_time']]);
            DB::table('order_pay_log')->insert(['order_number'=>$all['out_trade_no'],'log'=>serialize($all),'status'=>0,'pay_way_id'=>1]);
            return view('alipay.fail');
        }

        // 判断通知类型。
        switch (Input::get('trade_status')) {
            case 'TRADE_SUCCESS':
            case 'TRADE_FINISHED':
                // TODO: 支付成功，取得订单号进行其它相关操作。
                if($orderinfo->total_price == $all['total_fee']){
                   DB::table('orders')->where(['order_num'=>$all['out_trade_no']])->update(['pay_status'=>1,'pay_time'=>$all['notify_time'],'status'=>1]);
                    DB::table('user_info')->where(['id'=>$orderinfo->user_id])->update(['money'=>$orderinfo->price+$userinfo->money]);
                    $curl = Common::upMicSoft($userinfo,$orderinfo->price,3);
                    self::setLog(json_encode($curl).' 支付宝同步同步成功 --  订单号:'.$userinfo->soft_order_num.'--'.date('Y-m-d h:i:s'));
                    DB::table('order_pay_log')->insert(['order_number'=>$all['out_trade_no'],'log'=>serialize($all),'pay_way_id'=>1]);
                    $request->session()->forget('userid');
                    /*发送邮件*/
                    Common::sendMail($userinfo,1,$orderinfo); //用户
                    Common::sendMail($userinfo,2,$orderinfo); // 工作人员
                    // Common::sendMail($userinfo,1,$orderinfo->total_price); //用户
                    // Common::sendMail($userinfo,2,$orderinfo->total_price); // 工作人员
                }
                break;
        }
        return view('alipay.success');
    }

    // public function testemail(){
    //     $userinfo = DB::table('user_info')->where('soft_order_num', 111)->first();
    //     $orderinfo = DB::table('orders')->where('user_id', 1)->first();
    //     Common::sendMail($userinfo,2,$orderinfo); // 工作人员
    // }

    /**
     * 微软同步测试
     */
    public function test()
    {
        $url = 'http://mspil.chinacloudsites.cn/order/update';

        $data['order_number']= 'e5dfce71737e495caacd3f4fd1a94e87';
        $data['cellphone_number']= '13555555555';
        $data['email'] = 'test@qq.com';
        $data['name'] = 'test';
        $data['amount'] = 1100;
        $data['status'] = 2;
        $det = [
            'school'=>'test',
            'id_card_picurl'=>'baidu',
            'domain'=>'hello'
        ];
        $data['details'] = json_encode($det);
//        dd(json_encode($data));
        $res = Common::curl($url,json_encode($data));
        print_r(json_decode($res,true)['error']);
    }


    /**
     * 网银在线异步
     */
    public function payNotify(Request $request)
    {
        $all = $request->all();
        $ordernum = $all['remark1']; // 本系统自己的订单号
        $v_md5str = $all['v_md5str'];

        $v_oid = $all['v_oid'];
        $v_pstatus = $all['v_pstatus'];
        $v_amount = $all['v_amount'];
        $v_moneytype = $all['v_moneytype'];
        $key = Common::$key;

        $md5 = strtoupper(md5($v_oid.$v_pstatus.$v_amount.$v_moneytype.$key));
        if($v_md5str !== $md5 || $v_pstatus != 20){
//            return view();//todo 信息被修改
            die('error');
        }
        $order = DB::table('orders')->where(['order_num'=>$ordernum])->first();
        if($order && $order->pay_status == 1)die('ok');
        $userinfo = DB::table('user_info')->find($order->user_id);
        if($v_pstatus == 20 ){
                if($order->total != $v_amount){
//                    return view();//todo 订单金额和数据库的不一致
                    die('error');
                }else{
                    /*todo 更新数据库相关信息*/
                    $re = DB::table('orders')->where(['order_num'=>$ordernum,'pay_status'=>0])->update(['pay_status'=>1,'pay_time'=>time()]);
                    if($re){
//                        return view();//todo 更新成功页面
                        DB::table('order_pay_log')->insert(['order_number'=>$all['remark1'],'log'=>serialize($all),'pay_way_id'=>3,'status'=>1]);
                        $curl = Common::upMicSoft($userinfo,$order->price,3);
                        self::setLog(json_encode($curl).' 网银在线异步成功 --  订单号:'.$userinfo->soft_order_num.'--'.date('Y-m-d h:i:s'));
                        die('ok');
                    }else{
//                        return view();//todo 更新失败页面
                        DB::table('order_pay_log')->insert(['order_number'=>$all['remark1'],'log'=>serialize($all),'pay_way_id'=>3,'status'=>0]);
                        $curl = Common::upMicSoft($userinfo,$order->price,5);
                        self::setLog(json_encode($curl).' 网银在线异步失败--  订单号:'.$userinfo->soft_order_num.'--'.date('Y-m-d h:i:s'));
                        die('error');
                    }
                }
        }elseif($v_pstatus == 30){
//            return view();//todo 支付失败
            DB::table('order_pay_log')->insert(['order_number'=>$all['remark1'],'log'=>serialize($all),'pay_way_id'=>3,'status'=>0]);
            $curl = Common::upMicSoft($userinfo,$order->price,5);
            self::setLog(json_encode($curl).' 网银在线异步失败--  订单号:'.$userinfo->soft_order_num.'--'.date('Y-m-d h:i:s'));
            die('error');
        }else{
//            return view();//todo 未知错误
            die('error');
        }





    }
    /**
     * 网银在线同步
     */
    public function payResult(Request $request)
    {
        $all = $request->all();
        $ordernum = $all['remark1']; // 本系统自己的订单号
        $v_md5str = $all['v_md5str'];

        $v_oid = $all['v_oid'];
        $v_pstatus = $all['v_pstatus'];
        $v_amount = $all['v_amount'];
        $v_moneytype = $all['v_moneytype'];
        $key = Common::$Key;
        $md5 = strtoupper(md5($v_oid.$v_pstatus.$v_amount.$v_moneytype.$key));
        if($v_md5str !== $md5 || $v_pstatus != 20){
//            return view();//todo 信息被修改
            return view('alipay.fail',['data'=>'支付信息被非法修改!']);
        }
        $order = DB::table('orders')->where(['order_num'=>$ordernum,'pay_status'=>0])->first();
        if(!$order)die('error');
        $userinfo = DB::table('user_info')->find($order->user_id);
        if($v_pstatus == 20 ){
                if($order->total_price != $v_amount){
//                    return view();//todo 订单金额和数据库的不一致
                    return view('alipay.fail',['data'=>'订单金额和数据库的不一致']);
                }else{
                    /*todo 更新数据库相关信息*/
                    $re = DB::table('orders')->where(['order_num'=>$ordernum,'pay_status'=>0])->update(['pay_status'=>1,'pay_time'=>time()]);
                    if($re){
//                        return view();//todo 更新成功页面
                        DB::table('order_pay_log')->insert(['order_number'=>$all['remark1'],'log'=>serialize($all),'pay_way_id'=>3,'status'=>1]);
                        DB::table('user_info')->where(['id'=>$order->user_id])->update(['money'=>$order->price+$userinfo->money]);
                        $curl = Common::upMicSoft($userinfo,$order->price,3);
                        self::setLog(json_encode($curl).' 网银在线同步成功--  订单号:'.$userinfo->soft_order_num.'--'.date('Y-m-d h:i:s'));
                        /*发送邮件*/

                        // Common::sendMail($userinfo,1,$order->total_price); //用户
                        Common::sendMail($userinfo, 1, $order);

                        Common::sendMail($userinfo,2,$order); // 工作人员

                        return view('alipay.success',['data'=>'支付成功!']);
                    }else{
//                        return view();//todo 更新失败页面
                        DB::table('order_pay_log')->insert(['order_number'=>$all['remark1'],'log'=>serialize($all),'pay_way_id'=>3,'status'=>0]);
                        $curl = Common::upMicSoft($userinfo,$order->price,5);
                        self::setLog(json_encode($curl).' 网银在线同步失败--  订单号:'.$userinfo->soft_order_num.'--'.date('Y-m-d h:i:s'));
                        return view('alipay.fail',['data'=>'更新失败页面']);
                    }
                }
        }elseif($v_pstatus == 30){
//            return view();//todo 支付失败
            DB::table('order_pay_log')->insert(['order_number'=>$all['remark1'],'log'=>serialize($all),'pay_way_id'=>3,'status'=>0]);
            $curl = Common::upMicSoft($userinfo,$order->price,5);
            self::setLog(json_encode($curl).' 网银在线同步失败--  订单号:'.$userinfo->soft_order_num.'--'.date('Y-m-d h:i:s'));
            return view('alipay.fail',['data'=>'支付失败']);
        }else{
//            return view();//todo 未知错误
            return view('alipay.fail',['data'=>'未知错误']);
        }




    }








}