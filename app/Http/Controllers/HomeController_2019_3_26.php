<?php

namespace App\Http\Controllers;


use App\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function eee()
    {
        return view('errors.503');
    }
    public function first(Request $request)
    {
        /*
         * http://shit.com?order_number=1&cellphone_number=13555555555&email=110@xdf.com
         * http://azure.shop.edu.cn?order_number=1&cellphone_number=13555555555&email=110@xdf.com
         * http://azure.shop.edu.cn?order_number=订单号&cellphone_number=手机号&email=邮箱
         * */
        $all = $request->all();
        if(!isset($all['order_number']) || $all['order_number'] == ''){
            return redirect('/error');
        }
        if(!isset($all['cellphone_number']) || $all['cellphone_number'] == ''){
            return redirect('/error');
        }
        if(!isset($all['email']) || $all['email'] == ''){
            return redirect('/error');
        }

        session(['info'=>$all]) ;
        session(['flag'=>[1]]) ;
        return view('Home.index');
    }

    public function second()
    {
        // print_r(session('info'));die;
        $flag = session('flag');
        if(!$flag || !in_array(1,$flag)){
            return redirect('/error');
        }
        session(['flag'=>[1,2]]);
        return view('Home.second');
    }

    public function third()
    {
        $userid = session('userid');
        if($userid) return redirect('/last/'.$userid);

        $flag = session('flag');
        if(!$flag || !in_array(1,$flag) || !in_array(2,$flag)){
            return redirect('/error');
        }
        session(['flag'=>[1,2,3]]);
        $data = session('info');
        if(!$data) return redirect('/error');
        return view('Home.third',['data'=>$data]);
    }

    public function postThird(Request $request )
    {
        $input = $request->all();
        $info = DB::table('user_info')->where('soft_order_num',$input['soft_order_num'])->first();
        if($info){
            return -2;
        }
        $soft_order_num = $input['soft_order_num'];
        $name = $input['name'];
        $phone = $input['phone'];
        $id_card = '';//$input['id_card'];
        $email = $input['email'];
        $school = $input['school'];
        $money = $input['money'];
        $buytime = $input['buytime'];
        $inactive = $input['inactive'];
        $activeazureaccount = $input['activeazureaccount'];
        $moneytoazureaccount = $input['moneytoazureaccount'];
//        $domain_1 = $input['domain_1'].'.partner.onmschina.cn';
//        $domain_2 = $input['domain_2'].'.partner.onmschina.cn';
//        $domain_3 = $input['domain_3'].'.partner.onmschina.cn';
//        $domain_4 = $input['domain_4'].'.partner.onmschina.cn';
//        $domain_5 = $input['domain_5'].'.partner.onmschina.cn';

        $id_pic_url = '';//$input['WU_FILE_0'].','.$input['WU_FILE_1'];
        $domain = '';//$domain_1.';'.$domain_2.';'.$domain_3.';'.$domain_4.';'.$domain_5;
        $res = DB::table('user_info')
            ->insertGetId([
                'soft_order_num' => $soft_order_num,
                'name' => $name,
                'phone' => $phone,
                'id_card' => $id_card,
                'email' => $email,
                'school' => $school,
                'domain' => $domain,
                'id_card_picurl' => $id_pic_url,
                'money' => $money,
                'buytime' => $buytime,
                'inactive' => $inactive,
                'activeazureaccount' => $activeazureaccount,
                'moneytoazureaccount' => $moneytoazureaccount,
            ]);
        $userinfo = DB::table('user_info')->find($res);
        Mail::send('Mail.newinfo', ['res' => $userinfo], function ($message) {
            $message->from('lidaihong@163.com', 'Azure注冊系統提示');
            $message->to('chuzq@cernet.com');
            $message->to('wu.jun@microsoft.com');
            $message->to('luo.hong@microsoft.com');
            $message->to('wu.jun@microsoft.com');
            $message->to('v-mikli@microsoft.com');
            $message->cc('lidh@cernet.com');
            $message->subject('Azure注冊系統提示');
        });
        if($res){
            $request->session()->flash('userid',$res);
            return $res;
        }
        return -1;
    }

    public function last( $id )
    {
        $info = Common::getUserInfo($id);
        $userid = session('userid');
        if($userid != $id || !$info) return redirect('/error');
        // $flag = session('flag');
        // dd($flag);
        // if(!$flag || !in_array(1,$flag) || !in_array(2,$flag) || !in_array(3,$flag)){
        //     return redirect('/error');
        // }
        // session(['flag'=>[1,2,3,4]]);
        return view('Home.last',['info'=>$info]);
    }


    /**
     * webuploadify 图片上传
     * @param Request $request
     * @return string
     */
    public function doUpload(Request $request)
    {
        $all = $request->all();
        $file = $all['upload'];

        $destinationPath = 'storage/uploads/'.date('Ymd'); //public 文件夹下面建 storage/uploads 文件夹
        $extension = $file->getClientOriginalExtension();
        $fileName = str_random(10).'.'.$extension;
        $file->move($destinationPath, $fileName);
        $url = asset($destinationPath.'/'.$fileName);

//        dd($url);
        return json_encode(['url'=>$url]);
    }



}