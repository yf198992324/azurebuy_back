<?php


Route::get('/', 'HomeController@first');
Route::get('/second', 'HomeController@second');
Route::get('/third', 'HomeController@third');
Route::post('/third', 'HomeController@postThird');
Route::get('/last/{id}', 'HomeController@last')->where(['id'=>'[0-9]+']);
Route::get('/error', 'HomeController@eee');

Route::post('/doUpload', 'HomeController@doUpload');



Route::post('/do_order', 'PayController@doOrder');
Route::get('/topay/{id}', 'PayController@toPay')->where(['id'=>'[0-9]+']);

Route::post('/alipay/webnotify','PayController@webnotify');
Route::get('/alipay/webreturn','PayController@webreturn');



/*测试接口*/
Route::get('/test','PayController@test');
Route::get('/testemail','PayController@testemail');
Route::get('/paysucccess/test', function(){
    return view('alipay.success');
});
Route::get('/test/mailhtml', function(){
    return view('mailhtml1');
});


/*网银在线*/
Route::post('/order/payresult','PayController@payResult'); // 网银在线  同步
Route::post('/order/paynotify','PayController@payNotify'); // 网银在线  异步


/***************************  后台登录 *********************************************/

Route::get('/admin','Admin\HomeController@Login');
Route::get('/logout','Admin\HomeController@logout');
Route::post('/postlogin','Admin\HomeController@postLogin');


Route::group(['middleware'=>'auth'],function (){

    Route::get('/index','Admin\HomeController@Index');
    Route::get('/main','Admin\HomeController@Main');

    Route::get('/getnum','Admin\HomeController@getNum');

    Route::get('/user','Admin\UserController@index');
    Route::get('/pass','Admin\UserController@pass');
    Route::get('/nopass','Admin\UserController@noPass');


    Route::post('/checkpass','Admin\UserController@checkpass');
    Route::post('/changepass','Admin\UserController@changepass');



    Route::get('/userinfo','Admin\UserController@userInfo');
    Route::get('/changepwd','Admin\UserController@changePwd');

    Route::get('/downloadpic','Admin\UserController@downloadPic');

    Route::get('/order','Admin\OrderController@index');
    Route::get('/toregister','Admin\OrderController@registlist');
    Route::get('/topay','Admin\OrderController@paylist');
    Route::post('/torecharge','Admin\OrderController@torecharge');
    Route::get('/confirm','Admin\OrderController@confirmlist');
    Route::post('/toconfirm','Admin\OrderController@toconfirm');
    Route::post('/confirmMail','Admin\OrderController@confirmMail');

    Route::post('/toregist','Admin\OrderController@toregist');
    Route::post('/changeEmail','Admin\UserController@changeEmail');


    Route::get('/acount','Admin\OrderController@acountlist');

    Route::get('/downloaduser','Admin\ExcelController@downloadUser');
    Route::get('/downloadorder','Admin\ExcelController@downloadOrder');



    Route::get('/send','Admin\DealdetailController@test');



});
Route::get('/getssq','HController@index');
Route::get('/getnum','HController@getSsq');







