<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Common extends Model
{
    public static $Mid = '22392574';
    public static $ReturnUrl = "order/payresult"; // 同步
    public static $NotifyUrl = "order/paynotify"; // 异步
    public static $TradeSiteUrl ; //域名
    public static $Key = "fladjkflksadjf3940394f8f8e3";


    public static $manageEmail = [
                    "zhaohc@cernet.com",
                    "445282112@qq.com",
                    'lidaihong@163.com',
                    'chuzq@cernet.com',

                  ];



    public function __construct()
    {
        return self::$TradeSiteUrl = 'http://'.$_SERVER['SERVER_NAME'];
    }

    public static function getListByLevel($level)
    {
        $res = DB::table('service_good')->select(DB::raw('service_good.* ,service_category.service_name'))
            ->leftjoin('service_category', 'service_category.id', '=', 'service_good.service_category_id')
            ->where('service_good.level_id', $level)
            ->where('service_good.status', 1)
            ->get();
        return $res;
    }

    public static function formatData($data)
    {
        $return = [];
        foreach ($data as $k => $v) {
            $return[$v->service_category_id][] = $v;
        }
        return $return;
    }

    public static function getUserInfo($id)
    {
        return DB::table('user_info')->find($id);
    }

    public static function getOrderDetailById($id)
    {
        return DB::table('orders')->find($id);
    }


    public static function curl($url, $data, $ispost = 1)
    {
        $headers = array();
        $headers[] = 'Content-Type:application/json';
        $headers[] = 'Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMTEiLCJsZXZlbCI6IjUiLCJpc3MiOiJpc3N1ZXIiLCJhdWQiOiI5OTBhYzAwMjg2MWM0ODEyYjQyODMxZmJmYjMyM2ZkZCIsImV4cCI6MjEzNTI0ODMwOSwibmJmIjoxNTA0NTI4MzA5fQ.8t5jdzWzcyMmA2zwVyjd4H5rAFA1povmAMyTFGE9b0I';
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $url);//抓取指定网页
//        curl_setopt($ch, CURLOPT_HEADER, true);//设置header
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
        if ($ispost) {
            curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $back = curl_exec($ch);//运行curl
        curl_close($ch);
        return $back;
    }

    public static function upMicSoft($info,$price,$status)
    {
        $url = 'http://mspil.chinacloudsites.cn/order/update';
        $data['order_number'] = $info->soft_order_num;
        $data['cellphone_number'] = $info->phone;
        $data['email'] = $info->email;
        $data['name'] = $info->name;
        $data['amount'] = $price + $price * 0.1;
        $data['status'] = $status;
        $det = [
            'school'=>$info->school,
            'id_card_picurl'=>$info->id_card_picurl,
            'domain'=>$info->domain
        ];
        $data['details'] = json_encode($det);
        $curl = self::curl($url,json_encode($data));
        $curl = json_decode($curl,true);
        return $curl;
    }

    public  function GetChinaBankUrl($order)
    {
        $url = "https://Pay3.chinabank.com.cn/PayGate";

        $v_oid = date('Ymd' ,strtotime($order->created_at)).'-'.self::$Mid.'-'.$order->order_num;

        $info = $order->total_price . "CNY" . $v_oid . self::$Mid . (self::$TradeSiteUrl . "/" . self::$ReturnUrl) . self::$Key;
        $md5Info = strtoupper(md5($info));
        $url .= "?v_mid=" . self::$Mid;
        $url .= "&v_oid=" . $v_oid;
        $url .= "&v_amount=" . $order->total_price;
        $url .= "&v_moneytype=CNY";
        $url .= "&v_url=" . self::$TradeSiteUrl . "/" . self::$ReturnUrl;
        $url .= "&v_md5info=" . $md5Info;
        $url .= "&remark1=". $order->order_num;
        $url .= "&remark2=[url:=" . self::$TradeSiteUrl . "/" . self::$NotifyUrl . "]";
        $url .= "&v_rcvname=";
        $url .= "&v_rcvtel=";
        $url .= "&v_rcvpost=";
        $url .= "&v_rcvaddr=";
        $url .= "&v_rcvemail=";
        $url .= "&v_rcvmobile=";
        $url .= "&v_ordername=";
        $url .= "&v_ordertel=";
        $url .= "&v_orderpost=";
        $url .= "&v_orderaddr=";
        $url .= "&v_ordermobile=";
        $url .= "&v_orderemail=";

        return $url;
    }


    public static function sendMail( $userinfo,$flag=1 ,$order)
    {
        $money = $order->total_price;
        set_time_limit(0);
//        $email = '164735040@qq.com';
        $manageEmail = self::$manageEmail;
        // var_dump($order);exit;
        $account = $userinfo->moneytoazureaccount?$userinfo->moneytoazureaccount:$userinfo->activeazureaccount;
        $email = $userinfo->email;
        if($flag == 1){
            if($userinfo->inactive && $userinfo->activeazureaccount == $userinfo->moneytoazureaccount)
            {
                $temp = 'Mail.payisactive';
            }
            else {
                $temp = 'Mail.payisnotactive';
            }
            // $temp = 'Mail.one';
            $sub = '您的Azure产品订单购买申请以及支付流程已经完成';
        }elseif ($flag == 2){
            $email = $manageEmail;
            $temp = 'Mail.two';
            $sub = date('Y年m月d日 H:i:s').'有新用户申请购买Azure';
        }else{
            $temp = 'Mail.third';
            $sub = '您的Azure账户已经注册完成，请您及时登录账户系统并修改密码';
        }
        $flag = Mail::send($temp,['user'=>$userinfo,'order'=>$order, 'account' => $account, 'money'=> $money],function ($message) use ($email,$sub,$manageEmail){
           $message->to($email)->bcc($manageEmail)->subject($sub);
            // $message->to($email)->bcc()->subject($sub);
        });
        if($flag){
            return 1;
        }
        return 0;
    }


    // 1.   客户直接买，之前没有子订阅的情况: 或，用户在非活动账号里面重置
    public static function html1($money)
    {
        $html = '<!DOCTYPE html>
            <html>
                <head>
                    <title>Laravel</title>
                </head>
                <body>
                    <p style="text-align: left;">尊敬的微软教育用户：</p>
                    <div style="margin-left: 30px;">
                        <p>感谢您对微软教育的关注与支持！</p>

                        <p>我们已经收到的购买申请，您的充值金额为'.$money.'元，您可以获得的Azure 资源为'.$money.'*90%为 '.$money*0.9.'元。</p>
                        <br>
                        <hr> 
                        <br>
                        <p>我们接下来会通过电话联系您为您开通Azure 资源，资源开通后有效期为12个月，请留意接听客服电话!</p>
                        <br>
                        <p>在此, 为了帮助您更好的了解Azure的相关技术资源，给您提供以下学习微软Azure的资源网站，希望对您有帮助</p>
                        <p>1. 微软教育mspil网站视频学习地址:https://www.mspil.cn/#/lesson/9</p>
                        <p>2. Azure直播课堂: https://www.azure.cn/zh-cn/webinars/</p>
                        <p>3. Azure上云须知: https://www.azure.cn/zh-cn/webinars/ </p>
                        <p>4. Azure简介: https://docs.microsoft.com/zh-cn/learn/azure/</p>
                        <br>
                        <p>如果您有任何问题，可以直接回复邮件，我们会尽快答复您！</p>
                        <br>
                        <p>同时，您可以通过关注微软教育公众号和微软学生汇公众号了解更多关于微软教育的解决方案以及客户案例，期待您的关注!</p>
                        <br>
                        <p>
                            <img src="/images/weix1.png">
                        </p>
                    </div>
                </body>
            </html>';
    }
    // 2. 2.    客户在原有Azure 试用活动中购买情况
    public static function html2($money, $account)
    {
        $html = '<!DOCTYPE html>
            <html>
                <head>
                    <title>Laravel</title>
                </head>
                <body>
                    <p style="text-align: left;">尊敬的微软教育用户：</p>
                    <div style="margin-left: 30px;">
                        <p>感谢您对微软教育的关注与支持！</p>
                        <p>我们已经收到的购买申请，您的充值金额为'.$money.'元，您可以获得的Azure 资源为'.$money.'*90%为'.$money*0.9.'元</p>
                        <p>您现有的子订阅账号：'.$account.'</p>
                        <p>子订阅有效期: 2019年1月4日至2020年4月4日(延长12个自然月)</p>
                        <p>子订阅总共额度: 10000+'.$money.'元</p>
                        <br>
                        <p>特别提醒:</p>
                        <p>1.我们会在您使用额度达到10000元的时候进行邮件提醒</p>
                        <p>2.为了防止在不经意的情况下超额使用Azure资源造成不必要的损失，系统管理员会针对超额使用的用户的资源使用进行关闭和停止等操作，我们会以邮件或者电话方式提前通知用户，如果连续3个工作日无法联系到您，我们有权对分配给您的子订阅进行关闭等操作,子订阅关闭意味所有数据全部删除，无法保留</p>
                        <p>3.试用Azure资源仅限于测试目的，请勿存放敏感核心等业务数据</p>
                        <br>
                        <p>在此, 为了帮助您更好的了解Azure的相关技术资源，给您提供以下学习微软Azure的资源网站，希望对您有帮助</p>
                        <p>1. 微软教育mspil网站视频学习地址:https://www.mspil.cn/#/lesson/9</p>
                        <p>2. Azure直播课堂: https://www.azure.cn/zh-cn/webinars/</p>
                        <p>3. Azure上云须知: https://www.azure.cn/zh-cn/webinars/ </p>
                        <p>4. Azure简介: https://docs.microsoft.com/zh-cn/learn/azure/</p>
                        <br>
                        <p>如果您有任何问题，可以直接回复邮件，我们会尽快答复您！</p>
                        <br>
                        <p>同时，您可以通过关注微软教育公众号和微软学生汇公众号了解更多关于微软教育的解决方案以及客户案例，期待您的关注!</p>
                        <p>
                            <img src="/images/weix1.png">
                        </p>

                    </div>
                </body>
            </html>';
    }


    public static function insertLog($log)
    {
        $data['userid'] = Auth::user()->userid;
        $data['user_info_id'] = $log['uid'];
        $data['oid'] = $log['oid'];
        $data['content'] = $log['msg'];
        $data['type'] = $log['type'];
        DB::table('log')->insert($data);
    }




}