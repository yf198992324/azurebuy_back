<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{

    protected $table = 'user';
    public $primaryKey = 'userid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getUserList($input, $flag = false)
    {
        $user = DB::table('user_info');
        if (isset($input['keyword']) && $input['keyword'] != '') {
            $keyword = '%' . trim($input['keyword']) . '%';
            $user->where(function ($user) use ($keyword) {
                $user->orWhere("soft_order_num", 'like', $keyword)
                    ->orWhere('name', 'like', $keyword)
                    ->orWhere('id_card', 'like', $keyword)
                    ->orWhere('email', 'like', $keyword)
                    ->orWhere('phone', 'like', $keyword)
                    ->orWhere('school', 'like', $keyword)
                    ->orWhere('domain', 'like', $keyword);
            });
        }
        if (isset($input['time']) && $input['time'] != '') {
            $user->where('created_at', '>=', $input['time'] . ' 00:00:00')->where('created_at', '<=', $input['time'] . ' 59:59:59');
        }
        if (isset($input['status']) && $input['status'] != '-1' && $input['status'] != '') {
            $user->where('status', $input['status']);
        }
        $user->orderby('created_at', 'desc');
        if ($flag) {
            $users = $user->get();
        } else {
            $users = $user->paginate(10);
        }
        return $users;
    }


    public static function getOrderList($input, $flag = false, $status = null)
    {
        $order = DB::table('orders')
            ->select(DB::raw('orders.*,user_info.name,user_info.school,user_info.phone,user_info.id_card'))
            ->leftjoin('user_info', 'user_info.id', '=', 'orders.user_id');
        if (isset($input['keyword']) && $input['keyword'] != '') {
            $keyword = '%' . trim($input['keyword']) . '%';
            $order->where(function ($order) use ($keyword) {
                $order->orWhere("orders.order_num", 'like', $keyword)
                    ->orWhere('user_info.name', 'like', $keyword)
                    ->orWhere('user_info.school', 'like', $keyword)
                    ->orWhere('orders.soft_order_num', 'like', $keyword);
            });
        }
        if (isset($input['btime']) && $input['btime'] != '') {
            $order->where('orders.created_at', '>=', $input['btime']);
        }
        if (isset($input['etime']) && $input['etime'] != '') {
            $order->where('orders.created_at', '<=', $input['etime']);
        }
        if (isset($input['ptime']) && $input['ptime'] != '') {
            $order->where('orders.pay_status', '1')->where('orders.pay_time', '>=', $input['ptime'] . ' 00:00:00')->where('orders.pay_time', '<=', $input['ptime'] . ' 59:59:59');
        }
        if (isset($input['status']) && $input['status'] != '-1' && $input['status'] != '') {
            $order->where('orders.pay_status', $input['status']);
        }
        if (isset($input['payway']) && $input['payway'] != '-1' && $input['payway'] != '') {
            $order->where('orders.pay_way_id', $input['payway']);
        }
        if ($status) {
            if ($status == 4) {
                $order->where('orders.account_balance', '>', 0);
            }
            if ($status > 4) {
                $order->whereIn('orders.pay_status', [5, 6]);
            } else {
                $order->where('orders.pay_status', $status);
            }
//                $order->where('orders.pay_status',$status);
        }
        $order->orderby('created_at', 'desc');
        if ($flag) {
            $orders = $order->get();
        } else {
            $orders = $order->paginate(10);
        }
        return $orders;
    }

    public static function getAcountList($input)
    {
        $acountinfo = DB::table('user_order_acount')
            ->select(DB::raw('user_order_acount.*,user_info.name,user_info.email,user_info.phone'))
            ->leftjoin('user_info', 'user_info.id', '=', 'user_order_acount.user_info_id');
        if (isset($input['keyword']) && $input['keyword'] != '') {
            $keyword = '%' . trim($input['keyword']) . '%';
            $acountinfo->where(function ($acountinfo) use ($keyword) {
                $acountinfo->orWhere("user_order_acount.user_acount", 'like', $keyword)
                    ->orWhere('user_order_acount.user_pass', 'like', $keyword)
                    ->orWhere('user_info.name', 'like', $keyword)
                    ->orWhere('user_info.email', 'like', $keyword)
                    ->orWhere('user_info.phone', 'like', $keyword);
            });
        }
        $acountinfo->orderby('user_order_acount.created_at', 'desc');
        $acountinfos = $acountinfo->paginate(10);
        return $acountinfos;
    }

    public static function getConfirmList($input)
    {
        $order = DB::table('orders')
            ->select(DB::raw('orders.*,user_info.name,user_info.phone,user_info.email,user_order_acount.user_acount,user_order_acount.user_pass,user_order_acount.money,user_info.created_at as acount_create_time'))
            ->leftjoin('user_info', 'user_info.id', '=', 'orders.user_id')
            ->leftjoin('user_order_acount', 'user_order_acount.user_info_id', '=', 'orders.user_id');
        if (isset($input['keyword']) && $input['keyword'] != '') {
            $keyword = '%' . trim($input['keyword']) . '%';
            $order->where(function ($order) use ($keyword) {
                $order->orWhere("orders.order_num", 'like', $keyword)
                    ->orWhere('user_info.name', 'like', $keyword)
                    ->orWhere('user_info.email', 'like', $keyword)
                    ->orWhere('user_info.phone', 'like', $keyword)
                    ->orWhere('user_order_acount.user_acount', 'like', $keyword)
                    ->orWhere('user_order_acount.user_pass', 'like', $keyword);
            });
        }
        $order->where('orders.pay_status', '>', 4);
        return $order->paginate(10);
    }

    public function getAccountList($input,$page=true)
    {
        $order = DB::table('user_order_acount')
            ->select(DB::raw('user_order_acount.*,user_info.name,user_info.phone,user_info.email'))
            ->leftjoin('user_info', 'user_info.id', '=', 'user_order_acount.user_info_id');
        if (isset($input['keyword']) && $input['keyword'] != '') {
            $keyword = '%' . trim($input['keyword']) . '%';
            $order->where(function ($order) use ($keyword) {
                $order->orWhere("user_order_acount.user_acount", 'like', $keyword)
                    ->orWhere('user_info.name', 'like', $keyword)
                    ->orWhere('user_info.email', 'like', $keyword)
                    ->orWhere('user_info.phone', 'like', $keyword)
                    ->orWhere('user_order_acount.user_pass', 'like', $keyword);
            });
        }
        $order->where('user_order_acount.status', 1)->orderby('user_order_acount.created_at', 'desc');
        if($page){
            $orders = $order ->paginate(10);
        }else{
            $orders = $order ->get();
        }
        return $orders;
    }


}
